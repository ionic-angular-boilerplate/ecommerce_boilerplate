
### Guidelines for changing icon and splash screen
- Replace icon.png and splash.png in resources of mendatory size.

- Run `npm run resources` to generate custom icons and splash screens for your
app. 

### To implement this follow below mwntioned steps
- Run npm install cordova-res --save-dev
- Create 1024x1024px icon at resources/icon.png
- Create 2732x2732px splash at resources/splash.png
- Add "resources": "cordova-res ios && cordova-res android && node scripts/resources.js" to scripts in package.json
- Copy to script below to scripts/resources.js
- Run sudo chmod -R 777 scripts/resources.js
- Run npm run resources


###To know it's implementation steps see the folwing link
-Link: https://medium.com/@dalezak/generate-app-icon-and-splash-screen-images-for-ionic-framework-using-capacitor-e1f8c6ef0fd4

## Keep i mind
- For capacitor and cordova the path of generated ion/splash files becomes different. Check scripts/resources.js file.

Cordova reference documentation:

- Icons: https://cordova.apache.org/docs/en/latest/config_ref/images.html
- Splash Screens: https://cordova.apache.org/docs/en/latest/reference/cordova-plugin-splashscreen/
