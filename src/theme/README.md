To enable dark mode

1. Create custom variables in variable.scss file if you want to use your color theme.

2. We can detect if a browser supports dark-mode or not, Once we detect that the browser does or does not support dark-mode, we can attach a class .dark to <body> of the app. 

3. Inside app.component.ts or inside any service we put this in constructor
    // Use matchMedia to check the user preference
    const prefersDark = window.matchMedia('(prefers-color-scheme: dark)');

    toggleDarkTheme(prefersDark.matches);

    // Listen for changes to the prefers-color-scheme media query
    prefersDark.addListener((mediaQuery) => toggleDarkTheme(mediaQuery.matches));

    // Add or remove the "dark" class based on if the media query matches
    function toggleDarkTheme(shouldAdd) {
      document.body.classList.toggle('dark', shouldAdd);
    }

4. In variables.scss we provide values for all major CSS variables for all platforms inside a body.dark class


- Refer following link for more information.
https://medium.com/enappd/how-to-get-dark-mode-in-your-ionic-4-apps-in-15-mins-3b0a0e3c1032