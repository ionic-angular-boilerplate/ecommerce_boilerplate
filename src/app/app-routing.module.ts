import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { LoggedInAuthGuard, AuthGuard } from './shared/models/guards';

const routes: Routes = [
    {
        path: '',
        redirectTo: 'tabs',
        pathMatch: 'full'
    },
    {
        path: 'tabs',
        canActivate: [AuthGuard],
        loadChildren: () => import('./screens/tabs/tabs.module').then(m => m.TabsPageModule)
    },
    {
        path: 'login',
        // canActivate: [LoggedInAuthGuard],
        loadChildren: () => import('./screens/login/login.module').then(m => m.LoginPageModule)
    },

    {
        path: 'registration',
        // canActivate: [LoggedInAuthGuard],
        loadChildren: () => import('./screens/registration/registration.module').then(m => m.RegistrationPageModule)
    },
    {
        path: 'search',
        canActivate: [AuthGuard],
        loadChildren: () => import('./screens/search/search.module').then(m => m.SearchPageModule)
    },
    {
        path: 'settings',
        canActivate: [AuthGuard],
        loadChildren: () => import('./screens/settings/settings.module').then(m => m.SettingsPageModule)
    },
    {
        path: 'product-details',
        canActivate: [AuthGuard],
        loadChildren: () => import('./screens/product-details/product-details.module').then(m => m.ProductDetailsPageModule)
    },
    {
        path: 'product-list',
        canActivate: [AuthGuard],
        loadChildren: () => import('./screens/product/product-list/product-list.module').then(m => m.ProductListPageModule)
    },
    {
        path: 'cart',
        canActivate: [AuthGuard],
        loadChildren: () => import('./screens/cart/cart.module').then(m => m.CartPageModule)
    },
    {
        path: 'profile',
        loadChildren: () => import('./screens/profile/profile.module').then(m => m.ProfilePageModule)
    },
    {
        path: 'orders',
        loadChildren: () => import('./screens/orders/orders.module').then(m => m.OrdersPageModule)
    },
    {
        path: 'order-detail',
        loadChildren: () => import('./screens/order-detail/order-detail.module').then(m => m.OrderDetailPageModule)
    },
    {
        path: 'address',
        loadChildren: () => import('./screens/address/address.module').then(m => m.AddressPageModule)
    },
    {
        path: 'addresslist',
        loadChildren: () => import('./screens/addresslist/addresslist.module').then(m => m.AddresslistPageModule)
    },
    {
        path: 'checkout',
        loadChildren: () => import('./screens/checkout/checkout.module').then(m => m.CheckoutPageModule)
    }
    //   ,
    //   {
    //     path: 'faq',
    //     loadChildren: () => import('./faq/faq.module').then( m => m.FaqPageModule)
    //   },
];
@NgModule({
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }
