//Angular
import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
//Rxjs
import { of, from, EMPTY } from 'rxjs';
import { map, mergeMap, catchError, switchMap, tap, flatMap } from 'rxjs/operators';

//Store
import { Actions, createEffect, ofType, Effect } from '@ngrx/effects';
import * as ProfileStoreActions from './actions';
import { ProfileService } from '../../services/api/profileServices';

@Injectable()
export class ProfileEffect {

    constructor(private actions$: Actions,
        private router: Router,
        private profileService: ProfileService) { }
  
//get 
    public getOrderDetails$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileStoreActions.profile.GET_ORDER_DETAILS),
            mergeMap((action: ProfileStoreActions.getOrderDetails) =>
                this.profileService.getOrders().pipe(
                    switchMap((data: any) => {
                        console.log('getOrders', data);
                        return of(new ProfileStoreActions.getOrderDetailsSuccess({ orders: data }));
                    }),
                    catchError(error =>
                        of(new ProfileStoreActions.getOrderDetailsFail({ error: error.message || error.error }))
                    )
                )
            )
        )
    );
    public downloadInvoice$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProfileStoreActions.profile.INVOICE),
            mergeMap((action: ProfileStoreActions.Invoice) =>
                this.profileService.downloadInvoice(action.payload).pipe(
                    switchMap((data: any) => {
                        console.log('Invoice', data);
                        return of(new ProfileStoreActions.InvoiceSuccess({ receipt: data }));
                    }),
                    catchError(error =>
                        of(new ProfileStoreActions.InvoiceFail({ error: error.message || error.error }))
                    )
                )
            )
        )
    );

}