import { ProfileState } from './selectors';
import * as ProfileActions from './actions';
export const profileFeatureKey = 'profileFeature';

export const initialState: ProfileState = {
    receipt: [],
    orderDetails: [],
    isLoading: false,
    error: null
};

export function profileReducer(state: ProfileState = initialState, action: ProfileActions.actions) {
    switch (action.type) {
        
        case ProfileActions.profile.GET_ORDER_DETAILS: {
            return {
                ...state,
                isLoading: true
            }
        }
        case ProfileActions.profile.GET_ORDER_DETAILS_SUCCESS: {
            const orders = action.payload.orders.product_details
            return {
                ...state,
                orderDetails: orders,
                isLoading: false
            }
        }
        case ProfileActions.profile.GET_ORDER_DETAILS_FAIL: {
            const error = action.payload.error
            return {
                ...state,
                error: error,
                isLoading: false
            }
        }
        case ProfileActions.profile.INVOICE: {
            return {
                ...state,
                isLoading: true
            }
        }
        case ProfileActions.profile.INVOICE_SUCCESS: {
            const invoice = action.payload.receipt

            return {
                ...state,
                receipt: invoice,
                isLoading: false
            }
        }
        case ProfileActions.profile.INVOICE_FAIL: {
            const error = action.payload.error
            return {
                ...state,
                error: error,
                isLoading: false
            }
        }
        default:
            return state;
    }
}
