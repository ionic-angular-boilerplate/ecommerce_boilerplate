import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import * as fromAuth from './reducers';
import { ProfileEffect } from './effects';


@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature(fromAuth.profileFeatureKey, fromAuth.profileReducer),
        EffectsModule.forFeature([ProfileEffect])
    ],
    providers: []
})
export class ProfileModule { }
