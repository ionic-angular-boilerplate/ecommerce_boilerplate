import { createSelector, createFeatureSelector } from '@ngrx/store';

import { profileFeatureKey } from './reducers';

export interface ProfileState {
    receipt:any;
    orderDetails: any;
    isLoading: boolean;
    error: any;
}

export const selectFeature = createFeatureSelector<ProfileState>(profileFeatureKey);


export const selectOrderDetails = createSelector(
    selectFeature,
    (state: ProfileState) => state.orderDetails
);
export const selectInvoice = createSelector(
    selectFeature,
    // console.log()
    (state: ProfileState) => state.receipt
);
export const selectLoader = createSelector(
    selectFeature,
    // console.log()
    (state: ProfileState) => state.isLoading
);
