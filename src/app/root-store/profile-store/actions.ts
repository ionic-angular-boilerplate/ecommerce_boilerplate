import { Action } from "@ngrx/store";


export enum profile {
    GET_ORDER_DETAILS = '[GET_ORDER_DETAIL] Get order action!',
    GET_ORDER_DETAILS_SUCCESS = '[GET_ORDER_DETAIL_SUCCESS] Get order Success action!',
    GET_ORDER_DETAILS_FAIL = '[GET_ORDER_DETAIL_FAIL] Get order Fail action!',
    INVOICE = '[INVOICE] Download Invoice ',
    INVOICE_SUCCESS = '[INVOICE_SUCCESS] Download Invoice Success',
    INVOICE_FAIL = '[INVOICE_FAIL] Download Invoice Fail',
    NOOP_ACTION = '[Noop] Noop action'
    
}
// Get order details action
export class getOrderDetails implements Action {
    readonly type = profile.GET_ORDER_DETAILS;
}

// Get order details success action
export class getOrderDetailsSuccess implements Action {
    readonly type = profile.GET_ORDER_DETAILS_SUCCESS;
    constructor(public payload: any) { }
}

// Get order details fail action
export class getOrderDetailsFail implements Action {
    readonly type = profile.GET_ORDER_DETAILS_FAIL;
    constructor(public payload: any) { }
}

// Invoice Actions
export class Invoice implements Action {
    readonly type = profile.INVOICE;
    constructor(public payload: any) { }
}

// Invoice Success Actions
export class InvoiceSuccess implements Action {
    readonly type = profile.INVOICE_SUCCESS;
    constructor(public payload: any) { }
}

// Invoice fail Actions
export class InvoiceFail implements Action {
    readonly type = profile.INVOICE_FAIL;
    constructor(public payload: any) { }
}

export class NoopAction implements Action {
    readonly type = profile.NOOP_ACTION;
}


export type actions =
getOrderDetails
    | getOrderDetailsSuccess
    | getOrderDetailsFail
    | Invoice
    | InvoiceSuccess
    | InvoiceFail
    | NoopAction;


