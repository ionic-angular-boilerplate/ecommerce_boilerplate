import { environment } from '../../environments/environment';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { RouterStateSerializer } from '@ngrx/router-store';
import { StoreRouterConnectingModule } from '@ngrx/router-store';
import * as fromState from '../root-store/route-reducer'
import { AuthModule } from '.';
import { ProductModule } from './product-store';
import { ProfileModule } from './profile-store';
@NgModule({
    imports: [
        CommonModule,
        AuthModule,
        ProductModule,
        ProfileModule,
        StoreModule.forRoot(fromState.reducers),
        EffectsModule.forRoot([]),
        StoreRouterConnectingModule.forRoot(),
        StoreDevtoolsModule.instrument({
            maxAge: 25,
            logOnly: environment.production,
        })
    ],
    declarations: [],
    providers: [
        { provide: RouterStateSerializer, useClass: fromState.CustomSerializer }
    ]

})
export class RootStoreModule { }
