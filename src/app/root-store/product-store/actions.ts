import { Action } from "@ngrx/store";


export enum product {
    GET_CART_PRODUCT = '[GET_CART_PRODUCT] Get Cart action!',
    GET_CART_PRODUCT_SUCCESS = '[GET_CART_PRODUCT_SUCCESS] Get Cart Success action!',
    GET_CART_PRODUCT_FAIL = '[GET_CART_PRODUCT_FAIL] Get Cart Fail action!',
    DELETE_CART_PRODUCT = '[DELETE_CART_PRODUCT] Delete Cart Product action!',
    DELETE_CART_PRODUCT_SUCCESS = '[DELETE_CART_PRODUCT_SUCCESS] Delete Cart Product Success action!',
    DELETE_CART_PRODUCT_FAIL = '[DELETE_CART_PRODUCT_FAIL] Delete Cart Product Fail action!',
    GET_ALL_CATEGORY = '[GET_ALL_CATEGORY] Get all Category action!',
    GET_ALL_CATEGORY_SUCCESS = '[GET_ALL_CATEGORY_SUCCESS] Get all Category Success action!',
    GET_ALL_CATEGORY_FAIL = '[GET_ALL_CATEGORY_FAIL] Get all Category Product Fail action!',
    GET_TOP_RATED_PRODUCT = '[GET_TOP_RATED_PRODUCT] Get top rated product action!',
    GET_TOP_RATED_PRODUCT_SUCCESS = '[GET_TOP_RATED_PRODUCT_SUCCESS] Get top rated product success action!',
    GET_TOP_RATED_PRODUCT_FAIL = '[GET_TOP_RATED_PRODUCT_FAIL] Get top rated product fail action!',
    NOOP_ACTION = '[Noop] Noop action'
}
//Get product of all categories
export class getAllCategories implements Action {
    readonly type = product.GET_ALL_CATEGORY;
}
export class getAllCategoriesSuccess implements Action {
    readonly type = product.GET_ALL_CATEGORY_SUCCESS;
    constructor(public payload: any) { }
}
export class getAllCategoriesFail implements Action {
    readonly type = product.GET_ALL_CATEGORY_FAIL;
    constructor(public payload: any) { }
}

//Get tp rated product of all categories
export class getTopRatedProduct implements Action {
    readonly type = product.GET_TOP_RATED_PRODUCT;
}
export class getTopRatedProductSuccess implements Action {
    readonly type = product.GET_TOP_RATED_PRODUCT_SUCCESS;
    constructor(public payload: any) { }
}
export class getTopRatedProductFail implements Action {
    readonly type = product.GET_TOP_RATED_PRODUCT_FAIL;
    constructor(public payload: any) { }
}

// Add to Cart  Actions
export class getCartProduct implements Action {
    readonly type = product.GET_CART_PRODUCT;
}
export class getCartProductSuccess implements Action {
    readonly type = product.GET_CART_PRODUCT_SUCCESS;
    constructor(public payload: any) { }
}
export class getCartProductFail implements Action {
    readonly type = product.GET_CART_PRODUCT_FAIL;
    constructor(public payload: any) { }
}

// Delete from Cart Actions
export class deteleCartProduct implements Action {
    readonly type = product.DELETE_CART_PRODUCT;
    constructor(public payload: any) { }
}
export class deteleCartProductSuccess implements Action {
    readonly type = product.DELETE_CART_PRODUCT_SUCCESS;
    constructor(public payload: any) { }
}
export class deteleCartProductFail implements Action {
    readonly type = product.DELETE_CART_PRODUCT_FAIL;
    constructor(public payload: any) { }
}

export class NoopAction implements Action {
    readonly type = product.NOOP_ACTION;
}


export type actions =
    getCartProduct
    | getCartProductSuccess
    | getCartProductFail
    | deteleCartProduct
    | deteleCartProductSuccess
    | deteleCartProductFail
    | getAllCategories
    | getAllCategoriesSuccess
    | getAllCategoriesFail
    | getTopRatedProduct
    | getTopRatedProductSuccess
    | getTopRatedProductFail
    | NoopAction;


