import { ProductState } from './selectors';
import * as ProductActions from './actions';
export const productFeatureKey = 'producteature';

export const initialState: ProductState = {
    cartProduct: [],
    topRatedProduct: [],
    allCategoryProduct: [],
    isLoading: false,
    error: null
};

export function productReducer(state: ProductState = initialState, action: ProductActions.actions) {
    switch (action.type) {
        case ProductActions.product.GET_CART_PRODUCT: {
            return {
                ...state,
                isLoading: true
            }
        }
        case ProductActions.product.GET_CART_PRODUCT_SUCCESS: {
            const product = action.payload.products
            return {
                ...state,
                cartProduct: product,
                isLoading: false
            }
        }
        case ProductActions.product.GET_CART_PRODUCT_FAIL: {
            const error = action.payload.error
            return {
                ...state,
                error: error,
                isLoading: false
            }
        }
        case ProductActions.product.DELETE_CART_PRODUCT: {
            return {
                ...state,
                isLoading: true
            }
        }
        case ProductActions.product.DELETE_CART_PRODUCT_SUCCESS: {
            const product = action.payload.products
            return {
                ...state,
                cartProduct: product,
                isLoading: false
            }
        }
        case ProductActions.product.DELETE_CART_PRODUCT_FAIL: {
            const error = action.payload.error
            return {
                ...state,
                error: error,
                isLoading: false
            }
        }
        case ProductActions.product.GET_TOP_RATED_PRODUCT: {
            return {
                ...state,
                isLoading: true
            }
        }
        case ProductActions.product.GET_TOP_RATED_PRODUCT_SUCCESS: {
            const product = action.payload.products
            return {
                ...state,
                topRatedProduct: product,
                isLoading: false
            }
        }
        case ProductActions.product.GET_TOP_RATED_PRODUCT_FAIL: {
            const error = action.payload.error
            return {
                ...state,
                error: error,
                isLoading: false
            }
        }

        case ProductActions.product.GET_ALL_CATEGORY: {
            return {
                ...state,
                isLoading: true
            }
        }
        case ProductActions.product.GET_ALL_CATEGORY_SUCCESS: {
            const product = action.payload.products
            return {
                ...state,
                allCategoryProduct: product,
                isLoading: false
            }
        }
        case ProductActions.product.GET_ALL_CATEGORY_FAIL: {
            const error = action.payload.error
            return {
                ...state,
                error: error,
                isLoading: false
            }
        }

        default:
            return state;
    }
}
