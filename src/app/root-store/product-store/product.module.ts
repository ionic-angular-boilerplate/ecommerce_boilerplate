import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import * as fromAuth from './reducers';
import { ProductEffect } from './effects';


@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature(fromAuth.productFeatureKey, fromAuth.productReducer),
        EffectsModule.forFeature([ProductEffect])
    ],
    providers: []
})
export class ProductModule { }
