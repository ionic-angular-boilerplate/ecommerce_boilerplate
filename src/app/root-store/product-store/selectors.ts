import { createSelector, createFeatureSelector } from '@ngrx/store';

import { productFeatureKey } from './reducers';

export interface ProductState {
    cartProduct: any;
    topRatedProduct: any;
    allCategoryProduct: any;
    isLoading: boolean;
    error: any;
}

export const selectFeature = createFeatureSelector<ProductState>(productFeatureKey);

export const selectCartProduct = createSelector(
    selectFeature,
    (state: ProductState) => state.cartProduct
);

export const selectAllCategory = createSelector(
    selectFeature,
    (state: ProductState) => state.allCategoryProduct
);

export const selectTopRatedProduct = createSelector(
    selectFeature,
    (state: ProductState) => state.topRatedProduct
);
export const selectLoader = createSelector(
    selectFeature,
    // console.log()
    (state: ProductState) => state.isLoading
);