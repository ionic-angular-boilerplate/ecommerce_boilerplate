//Angular
import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
//Rxjs
import { of, from, EMPTY } from 'rxjs';
import { map, mergeMap, catchError, switchMap, tap, flatMap } from 'rxjs/operators';

//Store
import { Actions, createEffect, ofType, Effect } from '@ngrx/effects';
import * as ProductStoreActions from './actions';
import { ProductDetailsService } from '../../services/api/productServices';

@Injectable()
export class ProductEffect {

    constructor(private actions$: Actions,
        private router: Router,
        private productService: ProductDetailsService) { }
    // public addToCart$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(ProductStoreActions.auth.ADD_TO_CART),
    //         mergeMap((action: ProductStoreActions.Addticart) =>
    //             this.productService.addTocart(action.payload).pipe(
    //                 switchMap((data: any) => {
    //                     return of(new ProductStoreActions.AddticartSuccess({ data }));
    //                 }),
    //                 catchError(error =>
    //                     of(new ProductStoreActions.AddticartFail({ error: error.message || error.error }))
    //                 )
    //             )
    //         )
    //     )
    // );

    public getAllCategory$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductStoreActions.product.GET_ALL_CATEGORY),
            mergeMap((action: ProductStoreActions.getAllCategories) =>
                this.productService.getAllCategories().pipe(
                    switchMap((data: any) => {
                        console.log('getAllCategories', data);
                        return of(new ProductStoreActions.getAllCategoriesSuccess({ products: data.category_details }));
                    }),
                    catchError(error =>
                        of(new ProductStoreActions.getAllCategoriesFail({ error: error.message || error.error }))
                    )
                )
            )
        )
    );

    public getTopRatedProduct$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductStoreActions.product.GET_TOP_RATED_PRODUCT),
            mergeMap((action: ProductStoreActions.getTopRatedProduct) =>
                this.productService.getTopRatedProducts().pipe(
                    switchMap((data: any) => {
                        // console.log('getTopRatedProducts', data);
                        return of(new ProductStoreActions.getTopRatedProductSuccess({ products: data.product_details }));
                    }),
                    catchError(error =>
                        of(new ProductStoreActions.getTopRatedProductFail({ error: error.message || error.error }))
                    )
                )
            )
        )
    );

    public deteleCartProduct$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductStoreActions.product.DELETE_CART_PRODUCT),
            mergeMap((action: ProductStoreActions.deteleCartProduct) =>
                this.productService.deteleCartProduct(action.payload.id).pipe(
                    switchMap((data: any) => {
                        console.log('deteleCartProduct==>', data);
                        return of(new ProductStoreActions.getCartProductSuccess({ products: data.product_details }));
                    }),
                    catchError(error =>
                        of(new ProductStoreActions.getCartProductFail({ error: error.message || error.error }))
                    )
                )
            )
        )
    );

    public getCartProduct$ = createEffect(() =>
        this.actions$.pipe(
            ofType(ProductStoreActions.product.GET_CART_PRODUCT),
            mergeMap((action: ProductStoreActions.getCartProduct) =>
                this.productService.getCart().pipe(
                    switchMap((data: any) => {
                        console.log('getCartProduct', data);
                        return of(new ProductStoreActions.getCartProductSuccess({ products: data.product_details }));
                    }),
                    catchError(error =>
                        of(new ProductStoreActions.getCartProductFail({ error: error.message || error.error }))
                    )
                )
            )
        )
    );
}
