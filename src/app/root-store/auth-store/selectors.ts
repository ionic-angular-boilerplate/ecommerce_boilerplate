import { createSelector, createFeatureSelector } from '@ngrx/store';

import { authFeatureKey } from './reducers';

export interface AuthState {
    auth: any;
    user: any;
    isLogged: boolean;
    isLoginLoading: boolean;
    isRegisterLoading: boolean;
    isLoginError: any;
    isRegisterError: any;
    error: any;
}

export const selectFeature = createFeatureSelector<AuthState>(authFeatureKey);

export const selectAuth = createSelector(
    selectFeature,
    (state: AuthState) => state.auth
);


export const selectUser = createSelector(
    selectFeature,
    (state: AuthState) => state.user
);

export const selectUserStatus = createSelector(
    selectFeature,
    (state: AuthState) => state.isLogged
);

export const selectLoginIsLoading = createSelector(
    selectFeature,
    (state: AuthState) => state.isLoginLoading
);

export const selectRegisterIsLoading = createSelector(
    selectFeature,
    (state: AuthState) => state.isRegisterLoading
);

export const selectLoginError = createSelector(
    selectFeature,
    (state: AuthState) => state.isLoginError
);

export const selectRegisterError = createSelector(
    selectFeature,
    (state: AuthState) => state.isRegisterError
);

export const selectError = createSelector(
    selectFeature,
    (state: AuthState) => state.error
);
