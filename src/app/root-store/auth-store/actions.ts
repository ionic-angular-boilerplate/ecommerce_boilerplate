// import { createAction, props } from '@ngrx/store';
// import { HttpRequest, HttpHandler } from '@angular/common/http';
import { Action } from "@ngrx/store";


export enum auth {
    LOGIN = '[Login Component] Login!',
    LOGOUT = '[Logout Component] Logout!',
    LOGIN_SUCCESS = '[Login] Login Success',
    LOGIN_FAIL = '[Login] Login Fail',
    REGISTRATION = '[Registration Component] Registration!',
    REGISTRATION_SUCCESS = '[Registration] Registration Success',
    REGISTRATION_FAIL = '[Registration] Registration Fail',
    REHYDRATE_STATE = '[User] Rehydrate User state!',
    NOOP_ACTION = '[Noop] Noop action'
}

// Login Actions
export class Login implements Action {
    readonly type = auth.LOGIN;
    constructor(public payload: any) { }
}

// Login Success Actions
export class LoginSuccess implements Action {
    readonly type = auth.LOGIN_SUCCESS;
    constructor(public payload: any) { }
}

// Login failed Actions
export class LoginFail implements Action {
    readonly type = auth.LOGIN_FAIL;
    constructor(public payload: any) { }
}
// Registration Actions
export class Registration implements Action {
    readonly type = auth.REGISTRATION;
    constructor(public payload: any) { }
}

// Registration Success Actions
export class RegistrationSuccess implements Action {
    readonly type = auth.REGISTRATION_SUCCESS;
}

// Registration failed Actions
export class RegistrationFail implements Action {
    readonly type = auth.REGISTRATION_FAIL;
    constructor(public payload: any) { }
}

//Logout
export class Logout implements Action {
    readonly type = auth.LOGOUT;
}

// Registration failed Actions
export class NoopAction implements Action {
    readonly type = auth.NOOP_ACTION;
}

//Rehydrate user state
export class RehydrateState implements Action{
    readonly type = auth.REHYDRATE_STATE;
    constructor(public payload:any){}
}

export type actions = Login | Logout | RehydrateState |  LoginSuccess | LoginFail | Registration | RegistrationSuccess | RegistrationFail | NoopAction;

// Logout Actions
// export const logout = createAction('[Logout Component] Logout!');
// export const logoutSuccess = createAction('[Logout API] Logout');
// export const logoutFailed = createAction('[Logout API] Logout Failed!', props<{}>());

//Other
