import { AuthState } from './selectors';
import * as AuthActions from './actions';

export const authFeatureKey = 'authFeature';
export const initialState: AuthState = {
    auth: null,
    user: null,
    isLogged: false,
    isLoginLoading: false,
    isRegisterLoading: false,
    isLoginError: null,
    isRegisterError: null,
    error: null
};

export function authReducer(state: AuthState = initialState, action: AuthActions.actions) {
    // return reducer(state, action);
    switch (action.type) {

        case AuthActions.auth.LOGIN: {
            console.log('state',state)
            return {
                ...state,
                isLogged: false,
                isLoading: true,
            }
        }
        case AuthActions.auth.LOGIN_SUCCESS: {
            const _user = action.payload.data;
            return {
                ...state,
                isLogged: true,
                user: _user,
                isLoading: false,
            }
        }
        case AuthActions.auth.LOGIN_FAIL: {
            return {
                ...state,
                isLoading: false,
            }
        }
        case AuthActions.auth.REGISTRATION: {
            return {
                ...state,
                isLoading: false,
            }
        }
        case AuthActions.auth.REGISTRATION_SUCCESS: {
            return {
                ...state,
                isLogged: false,
                isLoading: false,
            }
        }
        case AuthActions.auth.REGISTRATION_FAIL: {
            return {
                ...state,
                isLoading: false,
            }
        }
        case AuthActions.auth.REHYDRATE_STATE: {
            const _user = action.payload.data;
            return {
                ...state,
                user: _user,
                isLogged: true
            };
        }
        case AuthActions.auth.LOGOUT: {
            return initialState;
        }

        default:
            return state;
    }
}
