import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import * as fromAuth from './reducers';
import { LoginEffect } from './effects';
import { AuthApiCallService } from '../../services/api/auth';


@NgModule({
    imports: [
        CommonModule,
        StoreModule.forFeature(fromAuth.authFeatureKey, fromAuth.authReducer),
        EffectsModule.forFeature([LoginEffect])
    ],
    providers: [AuthApiCallService]
})
export class AuthModule { }
