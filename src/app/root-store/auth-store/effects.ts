//Angular
import { Injectable } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
//Rxjs
import { of, from, EMPTY } from 'rxjs';
import { map, mergeMap, catchError, switchMap, tap, flatMap } from 'rxjs/operators';

//Store
import { Actions, createEffect, ofType, Effect } from '@ngrx/effects';
import * as AuthStoreActions from './actions';
import { Storage } from '@ionic/storage';
import { AuthApiCallService } from '../../services/api/auth';

@Injectable()
export class LoginEffect {

    constructor(private actions$: Actions,
        private authService: AuthApiCallService,
        private router: Router,
        private storage: Storage) { }
    public login$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthStoreActions.auth.LOGIN),
            mergeMap((action: AuthStoreActions.Login) =>
                this.authService.login(action.payload).pipe(
                    switchMap((data: any) => {
                        return of(new AuthStoreActions.LoginSuccess({ data }));
                    }),
                    catchError(error =>
                        of(new AuthStoreActions.LoginFail({ error: error.message || error.error }))
                    )
                )
            )
        )
    );

    @Effect({ dispatch: false })
    loginSucces$ = this.actions$.pipe(
        ofType<AuthStoreActions.LoginSuccess>(AuthStoreActions.auth.LOGIN_SUCCESS),
        tap(action => {
            console.log('body', action.payload.data);
            this.storage.set('authDetails', action.payload.data);
            this.router.navigate(['/tabs/home'])
        }),
    );

    @Effect({ dispatch: false })
    logout$ = this.actions$.pipe(
        ofType<AuthStoreActions.Logout>(AuthStoreActions.auth.LOGOUT),
        tap(action => {
            this.storage.remove('authDetails');
            console.log('inside logout');
            this.router.navigateByUrl('/login');
        }),
    );
    // public logout$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(AuthStoreActions.auth.LOGOUT),
    //         map(() => {
    //             this.storage.remove('authDetails');
    //             console.log('inside logout');
    //             this.router.navigate(['/login']);
    //             return new AuthStoreActions.NoopAction();
    //         })
    //     )
    // );


    // public loginFailed$ = createEffect(() =>
    //     this.actions$.pipe(
    //         ofType(AuthStoreActions.loginFailed),
    //         map(({ error }) => {
    //             // return AuthStoreActions.navigateToRoot();
    //         }),
    //     )
    // );

    public registration$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthStoreActions.auth.REGISTRATION),
            mergeMap((action: AuthStoreActions.Registration) =>
                this.authService.registration(action.payload).pipe(
                    map(() => new AuthStoreActions.RegistrationSuccess()),
                    catchError(error => of(new AuthStoreActions.RegistrationFail({ error: error.error.message || error.error })))
                )
            )
        )
    );

    public goRegistrationSuccess$ = createEffect(() =>
        this.actions$.pipe(
            ofType(AuthStoreActions.auth.REGISTRATION_SUCCESS),
            map((data) => {
                this.router.navigate(['/login']);
                return new AuthStoreActions.NoopAction();
            })
        )
    );

}
