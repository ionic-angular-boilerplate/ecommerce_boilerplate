import { Component, OnInit } from '@angular/core';
import { UitilityService } from '../../services/global services/uitility.service';
import { TranslateService } from '@ngx-translate/core';
import { PushNotificationService } from 'src/app/services/global services/push-notification.service';
import { GeneralService } from 'src/app/services/global services/general.service';

@Component({
    selector: 'app-settings',
    templateUrl: './settings.page.html',
    styleUrls: ['./settings.page.scss'],
})
export class SettingsPage implements OnInit {
    public isDarkmode: boolean = false;
    language: string;
    constructor(private utilityService: UitilityService,
        private translate: TranslateService,private pushService: PushNotificationService,
        private generalService: GeneralService) { }

    ngOnInit() {
        this.setDeviceDefaultLang();
    }

    changeTheme(event) {
        this.utilityService.enableDarkTheme(event.target.checked);

    }
    /** 
    * @description to get device's default language
   */
    setDeviceDefaultLang() {
        if (localStorage.getItem('locale')) {
            const browserLang = localStorage.getItem('locale');
            this.language = browserLang;
            // this.translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
        }
    }

    /** 
    * @description get called on chanage of the language
    * @param language: string language value e.g fr, en 
    **/
    changeLaguage(event) {
        localStorage.setItem('locale', event.target.value);
        this.language = event.target.value;
        this.translate.use(event.target.value);
    }
    triggerNotification(){
        this.pushService.displayLocalNotification()
    }
    demo(){
        console.log('function hits1')
    }
    
    // click(){
    //  this.generalService.presentAlert('Error','Error')
    //     // this.pushService.hit(this.demo)
    // }

}
