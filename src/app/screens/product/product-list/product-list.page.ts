import { Component, OnInit } from '@angular/core';
import { DatajsonService } from '../../../services/datajson.service';
import * as _ from 'lodash';
import { ActivatedRoute } from '@angular/router';
@Component({
    selector: 'app-product-list',
    templateUrl: './product-list.page.html',
    styleUrls: ['./product-list.page.scss'],
})
export class ProductListPage implements OnInit {
    product = [];
    title = 'Product List'
    constructor(private datajsonService: DatajsonService, private route: ActivatedRoute) { }

    ngOnInit() {
        const data = this.datajsonService.topRatedProduct.product_details
        this.route.queryParams.subscribe(params => {
            console.log(params)
            this.title = params.category_name
            this.product = _.filter(data, { 'category_id': params.category_id });
            console.log(this.product)
        })
    }

}
