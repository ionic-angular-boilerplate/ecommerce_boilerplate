import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";

import { ProductDetailsService } from 'src/app/services/api/productServices/product-details.service';
@Component({
  selector: "app-address",
  templateUrl: "./address.page.html",
  styleUrls: ["./address.page.scss"],
})
export class AddressPage implements OnInit {
  addressForm: FormGroup;
  submitted = false;
  addresslist: any;
  editAddres;

  constructor(
    private formBuilder: FormBuilder, 
    private route: Router,
    private productService:ProductDetailsService
    ) {}

  ngOnInit() {
    this.addressForm = this.formBuilder.group(
      {
        pincode: [
          "",
          [Validators.required, Validators.pattern("^[1-9][0-9]{5}$")],
        ],
        personName: ["", Validators.required],
        houseNo: ["", Validators.required],
        area: ["", Validators.required],
        city: [
          "",
          [
            Validators.required,
            Validators.pattern("^[a-zA-Z]+(?:[s-][a-zA-Z]+)*$"),
          ],
        ],
        state: [
          "",
          [
            Validators.required,
            Validators.pattern("^[a-zA-Z]+(?:[s-][a-zA-Z]+)*$"),
          ],
        ],
        mobile: [
          "",
          [Validators.required, Validators.pattern("^[6-9][0-9]{9}$")],
        ],
        landmark: [""],
        alternatemobile: ["", Validators.pattern("^[6-9][0-9]{9}$")],
        addressType: ["", Validators.required]
      }
    );
    this.editAddres=this.productService.editAddress;
    this.addresslist= JSON.parse(localStorage.getItem('addresslist'))||[];
    let temp=this.addresslist[this.editAddres];
    if(this.editAddres!=null){
      
      this.addressForm.controls['pincode'].setValue(temp.pincode);
      this.addressForm.controls['personName'].setValue(temp.personName);
      this.addressForm.controls['houseNo'].setValue(temp.houseNo);
      this.addressForm.controls['area'].setValue(temp.area);
      this.addressForm.controls['city'].setValue(temp.city);
      this.addressForm.controls['state'].setValue(temp.state);
      this.addressForm.controls['mobile'].setValue(temp.mobile);
      this.addressForm.controls['landmark'].setValue(temp.landmark);
      this.addressForm.controls['alternatemobile'].setValue(temp.alternatemobile);
      this.addressForm.controls['addressType'].setValue(temp.addressType);
    }
  }

  // convenience getter for easy access to form fields
  get f() {
    return this.addressForm.controls;
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.addressForm.invalid) {
      return;
    }

    if(this.editAddres!=null){
      this.addresslist= JSON.parse(localStorage.getItem('addresslist'))||[];
      this.addresslist.splice(this.editAddres, 1);  
      this.addresslist.push(this.addressForm.value);
      localStorage.setItem('addresslist',JSON.stringify(this.addresslist)); // store value in local storage 
      this.route.navigate(["addresslist"]);
    }
    else{
      this.addresslist = JSON.parse(localStorage.getItem("addresslist")) || [];
      let temp = this.addressForm.value;
      this.addresslist.push(temp);
      localStorage.setItem("addresslist", JSON.stringify(this.addresslist)); // store value in local storage
      this.route.navigate(["addresslist"]);
    }
    
  }
  editAddress() {

  }
}
