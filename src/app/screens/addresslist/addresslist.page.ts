import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';

import { ProductDetailsService } from 'src/app/services/api/productServices/product-details.service';
import {AddressPage} from '../address/address.page';

@Component({
  selector: 'app-addresslist',
  templateUrl: './addresslist.page.html',
  styleUrls: ['./addresslist.page.scss'],
})
export class AddresslistPage implements OnInit {
  @ViewChild(AddressPage) aadresspage: AddressPage;
  addresslist;
  selectedAddress;
  show: {[key: number]: boolean} = {};
  constructor(
    private route:Router,
    private productService:ProductDetailsService
  ) { }

  ngOnInit() {
  }
  ionViewWillEnter(){
    this.getAddress();
  }
  addAddress(){
    this.route.navigate(['address']);
  }
  checkValue(event){
    console.log(event.target.value,"radio check");
    for(let i=0;i<this.addresslist.length;i++){
      this.show[i]=false;
    }
    this.selectedAddress=event.target.value; 
    this.show[event.target.value] = true;
  }
  deliveredAddress(){
    this.productService.selectAddress=this.selectedAddress;
    this.route.navigate(['checkout']);
  }
  editAddress(index){
    console.log(index,"edit");    
    // this.addresslist= JSON.parse(localStorage.getItem('addresslist'))||[];
    // let temp=this.addresslist[index];
    this.productService.editAddress=index;
    //this.aadresspage.editAddress(temp);
    this.route.navigate(['address']);
  }
  deleteAddress(index){
    console.log(index,"delete");   
    this.addresslist= JSON.parse(localStorage.getItem('addresslist'))||[];
    this.addresslist.splice(index, 1);  
    localStorage.setItem('addresslist',JSON.stringify(this.addresslist)); // store value in local storage 
    this.getAddress();
  }
  getAddress(){
    this.addresslist= JSON.parse(localStorage.getItem('addresslist'))||[];   
  }
}
