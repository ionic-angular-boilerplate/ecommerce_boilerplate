import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ProfileStoreActions, ProfileStoreSelectors } from '../../root-store/profile-store';
import { Subscription } from 'rxjs';
import { GeneralService } from 'src/app/services/global services/general.service';
@Component({
    selector: 'app-orders',
    templateUrl: './orders.page.html',
    styleUrls: ['./orders.page.scss'],
})
export class OrdersPage implements OnInit {
    public orders: Subscription;
    constructor(
        private route: Router,
        private profileStore: Store<ProfileStoreSelectors.ProfileState>,
        private generalService: GeneralService
        ) { }

    ngOnInit() {
        this.profileStore.select(ProfileStoreSelectors.selectLoader).subscribe((data)=>{
            this.generalService.setLoaderStatus(data)
        console.log('lader status', data )

        })
         this.profileStore.select(ProfileStoreSelectors.selectOrderDetails).subscribe((response) => {
                if (response && response.length > 0) {
                    this.orders = response

                } else {
                    this.profileStore.dispatch(new ProfileStoreActions.getOrderDetails());
                }
            }
        )
        console.log('orders', this.orders)
    }
    navigateToDetail = (data) => {
        this.route.navigate(['/order-detail'],{ queryParams: { order_id: data._id}})
    }
    ngDestroy() {
        if (this.orders) {
            this.orders.unsubscribe();
        }
    }
}
