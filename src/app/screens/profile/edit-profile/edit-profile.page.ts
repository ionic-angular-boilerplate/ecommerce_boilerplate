import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-edit-profile',
  templateUrl: './edit-profile.page.html',
  styleUrls: ['./edit-profile.page.scss'],
})
export class EditProfilePage implements OnInit {
  profile_details = [
    {label:'First Name',value: 'Shubham'},
    {label:'Last Name',value: 'Soni'},
    {label:'Email',value: 'shubham.soni@neosofttech.com'},
    {label:'Mobile',value: '51234321'},
    {label:'DOB',value: '20-08-1997'},
    {label:'Gender',value: 'Male'}
  ]
  constructor() { }

  ngOnInit() {
  }

}
