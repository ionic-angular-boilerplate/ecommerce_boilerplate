import { Component, OnInit } from '@angular/core';
import { ActionSheetController, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { UitilityService } from 'src/app/services/global services/uitility.service';
@Component({
    selector: 'app-profile',
    templateUrl: './profile.page.html',
    styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
    profile_management = [
        
        {
            title: 'View Profile',
            route: 'profile/view-profile'
        },
        {
            title: 'My Orders',
            route: '/orders'
        },
        {
            title: 'Addresses',
            route: 'addresslist'
        },
        {
            title: 'Change Password',
            route: ''
        },
        // {
        //     title: 'Edit Profile',
        //     route: 'profile/edit-profile'
        // },
    ]
    constructor(private camera: Camera,
        public actionSheetController: ActionSheetController,
        public router: Router,
        private menu: MenuController, 
        private utility: UitilityService
        
        ) { }

    ngOnInit() {
    }
    
    navigate(route) {
        this.router.navigate([route])
    }
    changeProfileImage = () => {
        this.utility.openImgaeUploadOptions()
    }



}
