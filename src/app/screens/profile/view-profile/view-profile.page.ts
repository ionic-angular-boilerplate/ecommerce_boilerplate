import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AuthStoreSelectors } from 'src/app/root-store';
import { Subscription } from 'rxjs';
import { User } from 'src/app/shared/models';
import * as moment from 'moment';
import { GeneralService } from 'src/app/services/global services/general.service';
@Component({
    selector: 'app-view-profile',
    templateUrl: './view-profile.page.html',
    styleUrls: ['./view-profile.page.scss'],
})
export class ViewProfilePage implements OnInit {
    public profileSub: Subscription;
    userData: User;
    dob: String;
    constructor(private store: Store<AuthStoreSelectors.AuthState>,
        private generalService: GeneralService) {

    }

    ngOnInit() {
        this.profileSub = this.store.select(AuthStoreSelectors.selectUser).subscribe((data) => {
            this.store.select(AuthStoreSelectors.selectRegisterIsLoading).subscribe((data) => {
                this.generalService.setLoaderStatus(data)
                console.log('lader status', data)
            })


            if (data) {
                this.userData = data.customer_details;
                this.dob = moment(data.customer_details.dob).format("Do MMM YYYY")
            }
            console.log('==>', this.userData)
            this
        })
    }

}
