import { Component, OnInit } from '@angular/core';
import { DatajsonService } from '../../services/datajson.service';
import { Store } from '@ngrx/store';
import { ProductStoreSelectors, ProductStoreActions } from '../../root-store/product-store';
import { Subscription } from 'rxjs';
import { GeneralService } from '../../services/global services/general.service';
import { environment } from '../../../environments/environment';
@Component({
    selector: 'app-cart',
    templateUrl: './cart.page.html',
    styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {
    topRatedProduct = [];
    productSub: Subscription;
    deleteCartId: any;
    basePath = environment.CONFIG.BASE_URL;
    constructor(private datajsonService: DatajsonService,
        private productStore: Store<ProductStoreSelectors.ProductState>,
        private generalService: GeneralService
    ) { }

    ngOnInit() {
        // const productAddedToCart = this.datajsonService.cartProduct.product_details;
    }

    ngAfterViewInit() {
        this.productStore.dispatch(new ProductStoreActions.getCartProduct());
        this.productSub = this.productStore.select(ProductStoreSelectors.selectCartProduct).subscribe((data) => {
            if (data && data.length > 0) {
                // this.topRatedProduct = this.getFormattedProductList(data);
                this.topRatedProduct = this.getFormattedProductList(data);

            }
            else {
                this.topRatedProduct = [];
            }
        })
    }

    /** 
     * @description life cycle function to destroy subscription
    */
    ngDestroy() {
        if (this.productSub) {
            this.productSub.unsubscribe();
        }
    }

    /** 
     * @description function handles decrement event
    */
    decrementQty(index, qty) {
        if (qty > 1) {
            this.topRatedProduct[index].qty = qty - 1;
            this.topRatedProduct[index].product_display_cost = this.topRatedProduct[index].product_cost * this.topRatedProduct[index].qty;
        }
    }

    /** 
     * @description function handles increment event
    */
    incrementQty(index, qty) {
        if (qty <= 6) {
            this.topRatedProduct[index].qty = qty + 1;
            this.topRatedProduct[index].product_display_cost = this.topRatedProduct[index].product_cost * this.topRatedProduct[index].qty;
        }
        else {
            this.generalService.showToast('Maximum limit is 7', 'end');

        }
    }

    /** 
     * @description get formatted product list
    */
    getFormattedProductList(products) {
        const formattedProductList: any = [];
        products.forEach(element => {
            let ele = Object.create(element);
            ele['product_display_cost'] = ele['product_display_cost'] ? ele['product_display_cost'] : ele['quantity'] * ele.product_cost;
            ele['qty'] = ele['qty'] ? ele['qty'] : ele['quantity'];
            formattedProductList.push(ele);
        });
        // console.log('formattedProductList', formattedProductList);
        return formattedProductList;
    }

    /** 
     * @description get called on delete of item
    */
    deleteItem(item) {
        this.deleteCartId = item._id;
        this.generalService.presentAlertConfirm('Confirm', 'Are you sure want to delete this product from the cart', this.callback);
    }

    callback = (response) => {
        if (response) {
            this.productStore.dispatch(new ProductStoreActions.deteleCartProduct({ id: this.deleteCartId }));
        }
    }

    /** 
     * @description hook to get total of item
    */
    public totalAmount() {
        let total: number = 0;
        if (this.topRatedProduct.length > 0) {
            this.topRatedProduct.forEach((element) => {
                total = total + element.product_display_cost;
            })
        }
        return total;

    }

    routeProductPage(item) {

    }
}
