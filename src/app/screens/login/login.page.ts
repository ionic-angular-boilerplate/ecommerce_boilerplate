import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Regex } from '../../lib/regex'
import { AuthStoreSelectors, AuthStoreActions } from '../../root-store/auth-store';
import { Store } from '@ngrx/store';
import {
    AuthService,
    FacebookLoginProvider,
    GoogleLoginProvider
} from 'angular-6-social-login';
import { GeneralService } from 'src/app/services/global services/general.service';
@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
    styles: [`
        .bg-image {
            height: {{window.device.height}};
          width: {{window.device.width}};
        }
      `],
})
export class LoginPage implements OnInit {
    logo = '../../../assets/logo-transparent.png'
    loginForm: FormGroup;
    passwordType: any;
    isFieldValid = ''
    constructor(private router: Router,
        public formBuilder: FormBuilder,
        private authStore: Store<AuthStoreSelectors.AuthState>,
        private socialAuthService: AuthService,
        private generalService: GeneralService
        ) {
        // constructor(private router: Router, public formBuilder: FormBuilder, private socialAuthService: AuthService) {
        this.loginForm = this.formBuilder.group({
            email: ['', [Validators.required, Validators.pattern(Regex.EMAIL_REGX)]],
            pass: ['', [Validators.required, Validators.pattern(Regex.PASSWORD)]],
            // captcha: ['', Validators.required]
        });

    }

    async ngOnInit() {
        this.passwordType = 'password';
        await this.authStore.select(AuthStoreSelectors.selectLoginIsLoading).subscribe((data)=>{
            this.generalService.setLoaderStatus(data)
            console.log('lader status', data )
          })
    }

    /** 
     * @description function get calls on successfully submitting the login form.
     * It handles login API integration with store.
    */
    onSubmit() {
        if (!this.loginForm.valid) {
            this.isFieldValid = 'ng-invalid in-item hydrated ng-touched ion-pristine ion-invalid ion-touched'
            this.loginForm.markAllAsTouched()
            return;
        }
        const email = this.loginForm.value.email;
        const pass = this.loginForm.value.pass;
        // this.router.navigateByUrl('/home');

        // calling login action with ngrx/store 
        this.authStore.dispatch(new AuthStoreActions.Login({ email, pass }));

        //  this.router.navigate(['/tabs/home'])
    }

    showPassword() {
        this.passwordType = this.passwordType === 'password' ? this.passwordType = 'text' : this.passwordType = 'password';
    }
    public socialSignIn(socialPlatform: string) {
        let socialPlatformProvider;
        if (socialPlatform == "facebook") {
            socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
        } else if (socialPlatform == "google") {
            socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
        }

        this.socialAuthService.signIn(socialPlatformProvider).then(
            (userData) => {
                console.log(socialPlatform + " sign in data : ", userData);
                // Now sign-in with userData
                // ...

            }
        );
    }


}
