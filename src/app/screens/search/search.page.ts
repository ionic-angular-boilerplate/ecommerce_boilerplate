import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-search',
    templateUrl: './search.page.html',
    styleUrls: ['./search.page.scss'],
})
export class SearchPage implements OnInit {
    isItemAvailable = false;
    items = ["Ant-Man",
        "Aquaman",
        "Asterix",
        "The Atom",
        "The Avengers",
        "Batgirl",
        "Batman",
        "Batwoman",
        "Black Canary",
        "Black Panther",
        "Captain America",
        "Captain Marvel",
        "Catwoman",
        "Conan the Barbarian",
        "Daredevil",
        "The Defenders",
        "Doc Savage",
        "Doctor Strange",
        "Fantastic Four",
        "Ghost Rider",
        "Green Arrow",
        "Green Lantern",
        "Guardians of the Galaxy",
        "Hawkeye",
        "Hellboy",
        "Incredible Hulk",
        "Iron Fist",
        "Iron Man",
        "Spider-Man",
        "Supergirl",
        "Superman",
        "Teenage Mutant Ninja Turtles",
        "Thor",
        "The Wasp",
        "Wolverine",
        "Wonder Woman",
        "X-Men"
    ];

    constructor() { }

    ngOnInit() {
    }
    initializeItems() {
        this.items = ["Ant-Man",
            "Aquaman",
            "Asterix",
            "The Atom",
            "The Avengers",
            "Batgirl",
            "Batman",
            "Batwoman",
            "Black Canary",
            "Black Panther",
            "Captain America",
            "Captain Marvel",
            "Catwoman",
            "Conan the Barbarian",
            "Daredevil",
            "The Defenders",
            "Doc Savage",
            "Doctor Strange",
            "Fantastic Four",
            "Ghost Rider",
            "Green Arrow",
            "Green Lantern",
            "Guardians of the Galaxy",
            "Hawkeye",
            "Hellboy",
            "Incredible Hulk",
            "Iron Fist",
            "Iron Man",
            "Spider-Man",
            "Supergirl",
            "Superman",
            "Teenage Mutant Ninja Turtles",
            "Thor",
            "The Wasp",
            "Wolverine",
            "Wonder Woman",
            "X-Men"]
    }
    filter(ev: any) {
        // Reset items back to all of the items
        this.initializeItems();

        // set val to the value of the searchbar
        const val = ev.target.value;

        // if the value is an empty string don't filter the items
        if (val && val.trim() !== '') {
            this.isItemAvailable = true;
            this.items = this.items.filter((item) => {
                return (item.toLowerCase().indexOf(val.toLowerCase()) > -1);
            })
        } else {
            this.isItemAvailable = false;
        }
    }

}
