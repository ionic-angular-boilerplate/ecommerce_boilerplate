import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { DatajsonService } from '../../services/datajson.service';
import { ProductStoreActions, ProductStoreSelectors } from '../../root-store/product-store';
import { Store } from '@ngrx/store';
import { ProductDetailsService } from '../../services/api/productServices';
import { environment } from '../../../environments/environment';
import { TopRatedProducts, AllCategoryProduct } from '../../shared/models/interfaces/product';
import { ProfileStoreSelectors } from 'src/app/root-store/profile-store';
import { GeneralService } from 'src/app/services/global services/general.service';

@Component({
    selector: 'app-home',
    templateUrl: './home.page.html',
    styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {

    basePath = environment.CONFIG.BASE_URL;
    slideData: AllCategoryProduct[] = [];
    topRatedProduct: TopRatedProducts[] = [];
    totalTopRatedProducts: TopRatedProducts[] = [];
    listView = false
    gridView = true
    index = 0;
    limit = 5;
    total;
    constructor(
        private productStore: Store<ProductStoreSelectors.ProductState>,
        private datajsonService: DatajsonService,
        private router: Router,
        private productDetailsService: ProductDetailsService,
        private generalService: GeneralService

    ) { }
    slideOptsForCategory = {
        slidesPerView: 5,
        initialSlide: 0,
        speed: 400,
        pagination: false
    };
    slideOptsForProduct = {
        slidesPerView: 1,
        initialSlide: 0,
        speed: 400,
        pagination: false,
        updateOnWindowResize: true,
        autoplay: true
    };

    /** 
     * @description get called on initiation of the component
    */
    ngOnInit() {
        // this.productCategory = this.datajsonService.productCategory;
        // this.slideData = this.productCategory.category_details; //slides data
        // this.generalService.presentAlertConfirm()
        // console.log(a)
        // this.topRatedProduct = this.datajsonService.topRatedProduct.product_details; // top rated product


        //handling all category products 
        this.productStore.select(ProfileStoreSelectors.selectLoader).subscribe((data)=>{
            this.generalService.setLoaderStatus(data)
        console.log('lader status', data )
        })
        this.productStore.select(ProductStoreSelectors.selectAllCategory).subscribe((response) => {
            console.log('selectAllCategory', response);
            if (response && response.length > 0) {
                this.slideData = response
            }
            else {
                this.productStore.dispatch(new ProductStoreActions.getAllCategories());
            }
        })

        //handling top rated products
        this.productStore.select(ProductStoreSelectors.selectTopRatedProduct).subscribe((response) => {
            console.log('selectTopRatedProduct', response);
            if (response && response.length > 0) {
                this.total = response.length;
                this.totalTopRatedProducts = response;
                this.fetchData(this.index, this.limit);
            }
            else {
                this.productStore.dispatch(new ProductStoreActions.getTopRatedProduct());
            }
        })
    }

    /** 
     * @description navigates to search page
    */
    openSearch() {
        this.router.navigate(['/search'])
    }

    /** 
     * @description to enable list view and disabled grid view
    */
    toggleListView() {
        this.listView = true;
        this.gridView = false;
    }

    /** 
     * @description to enable grid view and disabled list view
    */
    toggleGridView() {
        this.gridView = true;
        this.listView = false
    }

    /**
     * @description get called after refreshing the page
     * @param refresher reference of refresher
     */
    doRefresh(refresher) {
        setTimeout(() => {
            console.log('Async operation has ended');
            refresher.detail.complete();
        }, 2000);
    }

    /**
     * @description to navigate over the details page of the product
     * @param item 
     */
    routeProductDetailPage(item) {
        this.productDetailsService.productID = item._id;
        this.router.navigate(['product-details']);
        console.log(item._id);
    }

    /**
     * @description to get data from the API of to rated products. Fetched 5 items by default
     * @param index : number
     * @param limit : number limit of fetch records
     */
    fetchData(index?, limit?) {
        // console.log(index, limit);
        for (let i = index; i < limit; i++) {
            const product = this.totalTopRatedProducts[i];
            this.topRatedProduct.push(product)
        }
    }

    /**
     * @description get called on infinite scroll event. Requesting next 5 records.
     * @param event scroll event
     */
    loadData(event) {
        this.index = this.index + 5
        this.limit = this.limit + 5
        setTimeout(() => {

            // load more data
            this.fetchData(this.index, this.limit)
            //Hide Infinite List Loader on Complete
            event.target.complete();

            // App logic to determine if all data is loaded
            // and disable the infinite scroll
            if (this.topRatedProduct.length == this.total) {
                event.target.disabled = true;
            }
        }, 2000);
    }

    /**
     * @description navigates to the product list as per the category
     * @param id : category id 
     * @param name : category name
     */
    navigateToProduct(id, name) {
        this.router.navigate(['/product-list/'], { queryParams: { category_id: id, category_name: name } });
    }


}
