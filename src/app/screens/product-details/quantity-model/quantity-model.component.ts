import { Component, OnInit } from '@angular/core';
import { PopoverController, } from '@ionic/angular';
@Component({
    selector: 'app-quantity-model',
    templateUrl: './quantity-model.component.html',
    styleUrls: ['./quantity-model.component.scss'],
})
export class QuantityModelComponent implements OnInit {
    quantity: string[] = ['1', '2', '3', '4', '5'];
    productQuantity: number;
    error: string = '';
    constructor(private popoverController: PopoverController) { }

    ngOnInit() {
        this.error = '';
    }

    //selection event of quantity popover
    onQuantitySelection(event) {
        console.log('onQuantitySelection', event.detail.value);
        this.productQuantity = event.detail.value;
        // if (this.error.length > 0) {
        //     this.error = '';
        // }
    }

    //on closing popover
    onClose() {
        this.popoverController.dismiss();
    }

    //on click of okay of the popover
    onOkay() {
        if (this.productQuantity) {
            this.popoverController.dismiss(this.productQuantity);
        }
        else {
            this.error = 'Select the quantity';
        }
    }

}
