import { Component, OnInit } from '@angular/core';

import { environment } from '../../../environments/environment';
import { ProductDetailsService } from '../../services/api/productServices';
import { Router } from '@angular/router';
import { ActionSheetController, PopoverController } from '@ionic/angular';
import { SocialSharing } from "@ionic-native/social-sharing/ngx";
import { QuantityModelComponent } from './quantity-model/quantity-model.component';
import { GeneralService } from '../../services/global services/general.service';
@Component({
    selector: 'app-product-details',
    templateUrl: './product-details.page.html',
    styleUrls: ['./product-details.page.scss'],
})
export class ProductDetailsPage implements OnInit {

    productDetail;
    subImages;
    baseUrl = environment.CONFIG.BASE_URL;
    share_msg = 'Check this product on boilerplate';
    img_URL;
    shareurl;
    productRating;
    currentPopover = null;
    constructor(
        private productDetailsService: ProductDetailsService,
        private socialSharing: SocialSharing,
        private popovercontroller: PopoverController,
        public actionSheetController: ActionSheetController,
        private router: Router,
        private generalService: GeneralService) { }

    ngOnInit() {
        let productId = this.productDetailsService.productID;
        this.productDetailsService
            .accessProductDetails(productId) // access product details of product
            .subscribe((data: any) => {
                this.productDetail = data.product_details[0];
                this.subImages = data.product_details[0].subImages_id.product_subImages;
                this.img_URL = this.baseUrl + data.product_details[0].subImages_id.product_subImages[0];
                if (data.product_details[0].product_rating == 'NaN') {
                    this.productRating = 4.1;
                }
                else {
                    this.productRating = Number((data.product_details[0].product_rating)).toFixed(1)
                }
                console.log(this.productDetail, "productDetails");
            });
    }

    // function for sharing product on social media
    async shareProduct() {
        const actionSheet = await this.actionSheetController.create({
            //create action sheet
            header: "Share with",
            cssClass: "actionpage",
            buttons: [
                {
                    text: "Whatsup",
                    icon: "logo-whatsapp",
                    cssClass: "logowhatsapp",
                    handler: () => {
                        this.shareWhatsApp(); // share on whatsapp
                    }
                },
                {
                    text: "Facebook",
                    icon: "logo-facebook",
                    cssClass: "logofacebook",
                    handler: () => {
                        this.shareFacebook(); //share on facebook
                    }
                },
                {
                    text: "Gmail",
                    icon: "mail",
                    cssClass: "logomail",
                    handler: () => {
                        this.shareEmail(); //share on gmail
                    }
                },
                {
                    text: "Twitter",
                    icon: "logo-twitter",
                    cssClass: "logotwitter",
                    handler: () => {
                        this.shareTwitter(); //share on twitter
                    }
                },
                {
                    text: "Instagram",
                    icon: "logo-instagram",
                    cssClass: "logoinstagram",
                    handler: () => {
                        this.shareInstagram(); //share on instagram
                    }
                },
                {
                    text: "Cancel",
                    role: "cancel"
                }
            ]
        });
        await actionSheet.present();
    }

    async shareTwitter() {
        // Either URL or Image
        this.socialSharing
            .shareViaTwitter(this.share_msg, this.img_URL[0], this.shareurl)
            .then(() => {
                // Success
            })
            .catch(e => {
                // Error!
            });
    }

    async shareWhatsApp() {
        // Text + Image or URL works
        this.socialSharing
            .shareViaWhatsApp(this.share_msg, this.img_URL[0], this.shareurl)
            .then(() => {
                // Success
            })
            .catch((e: any) => {
                // Error!
            });
    }

    async shareEmail() {
        this.socialSharing
            .shareViaEmail(null, "My custom subject", ["abc@gmail.com"], null, null, this.img_URL[0])
            .then(() => {
                //this.removeTempFile(file.name);
            })
            .catch(e => {
                // Error!
            });
    }

    async shareFacebook() {
        // Image or URL works
        this.socialSharing
            .shareViaFacebook(this.share_msg, this.img_URL[0], this.shareurl)
            .then(() => { })
            .catch(e => {
                // Error!
            });
    }

    async shareInstagram() {
        // Image or URL works
        this.socialSharing
            .shareViaInstagram(this.share_msg, this.img_URL[0])
            .then(() => { })
            .catch(e => {
                // Error!
            });
    }


    async handleButtonClick(ev) {
        const popover = await this.popovercontroller.create({
            component: QuantityModelComponent,
            event: ev,
            translucent: true
        });

        popover.onDidDismiss().then((result) => {

            if (result && result.data) {
                let payload = {
                    product_id: this.productDetail._id,
                    quantity: + result.data
                }
                // let formData = new FormData();
                // Object.keys(payload).forEach((ele, index) => {
                //     formData.append(ele, payload[ele]);
                // })
                this.productDetailsService.addToCart(payload).subscribe((response) => {
                    console.log('addToCart=>', response);
                    if (response.success && response.message) {
                        this.generalService.showToast(response.message, 'end');
                    }
                }, (error) => {
                    console.error('addToCart=>', error);

                })
            }
        });
        return await popover.present();


    }



    gotoCheckout() {
        this.router.navigate(['checkout']);
    }
}
