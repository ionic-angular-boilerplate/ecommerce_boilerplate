import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';


import { ProductDetailsPageRoutingModule } from './product-details-routing.module';

import { ProductDetailsPage } from './product-details.page';
import { QuantityModelComponent } from './quantity-model/quantity-model.component';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        ProductDetailsPageRoutingModule,
    ],
    declarations: [ProductDetailsPage, QuantityModelComponent],
    providers: [SocialSharing],
    entryComponents: [QuantityModelComponent]
})
export class ProductDetailsPageModule { }
