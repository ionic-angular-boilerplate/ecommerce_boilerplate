import { Component, OnInit } from '@angular/core';
import { ProductDetailsService } from 'src/app/services/api/productServices/product-details.service';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ProductStoreSelectors, ProductStoreActions } from '../../root-store/product-store';
import { Subscription } from 'rxjs';
import { environment } from '../../../environments/environment';

@Component({
    selector: 'app-checkout',
    templateUrl: './checkout.page.html',
    styleUrls: ['./checkout.page.scss'],
})
export class CheckoutPage implements OnInit {

    address;
    addressarray;
    cartItem;
    cartCount: number = 1;
    totalAmount = 0;
    totalPayble = 0;
    charges = 0;
    quantity: string[] = ['1', '2', '3', '4', '5'];
    freeDelivery: boolean = false;
    selectAddress = "Select Or Enter Address";
    products: any = [];
    basePath = environment.CONFIG.BASE_URL;

    // products = [{
    //     "productName": "Nova Prime Series",
    //     "color": "black",
    //     "producer": "Amazestore",
    //     "mrpPrice": 1705,
    //     "sellingPrice": 1350,
    //     "discoutPrice": 355,
    //     "discountPercent": 17,
    //     "deliveryDays": "5-7",
    //     "deliverytype": "free",
    //     "deliveryCharge": 40,
    //     "quantity": "1",
    //     "productImage": "https://images-eu.ssl-images-amazon.com/images/I/31K0fLdeuJL._SY300_QL70_ML2_.jpg"
    // },
    // {
    //     "productName": "Nova Prime Series",
    //     "color": "black",
    //     "producer": "Amazestore",
    //     "mrpPrice": 1705,
    //     "sellingPrice": 1350,
    //     "discoutPrice": 355,
    //     "discountPercent": 17,
    //     "deliveryDays": "5-7",
    //     "deliverytype": "free",
    //     "deliveryCharge": 40,
    //     "quantity": "2",
    //     "productImage": "https://images-eu.ssl-images-amazon.com/images/I/31K0fLdeuJL._SY300_QL70_ML2_.jpg"
    // },
    // {
    //     "productName": "Nova Prime Series",
    //     "color": "black",
    //     "producer": "Amazestore",
    //     "mrpPrice": 1705,
    //     "sellingPrice": 1350,
    //     "discoutPrice": 355,
    //     "discountPercent": 17,
    //     "deliveryDays": "5-7",
    //     "deliverytype": "free",
    //     "deliveryCharge": 40,
    //     "quantity": "3",
    //     "productImage": "https://images-eu.ssl-images-amazon.com/images/I/31K0fLdeuJL._SY300_QL70_ML2_.jpg"
    // }
    // ];
    selectedClassification = {
        item: null
    };
    productSub: Subscription;
    constructor(
        private route: Router,
        private productService: ProductDetailsService,
        private productStore: Store<ProductStoreSelectors.ProductState>,
    ) {
        this.selectedClassification.item = 1;
    }

    ngOnInit() {
        this.productStore.dispatch(new ProductStoreActions.getCartProduct());
        this.productSub = this.productStore.select(ProductStoreSelectors.selectCartProduct).subscribe((data) => {
            if (data && data.length > 0) {
                // this.topRatedProduct = this.getFormattedProductList(data);
                this.products = data;

            }
            else {
                this.products = [];
            }
        })
    }


    ionViewWillEnter() {
        let addressindex = this.productService.selectAddress;
        this.addressarray = JSON.parse(localStorage.getItem('addresslist')) || [];
        this.address = this.addressarray[addressindex];
        //this.address=this.addressarray[1];
        console.log(this.address, "address checkout");
        this.products.forEach(element => {
            this.totalAmount = element.sellingPrice + this.totalAmount;
            this.totalPayble = this.totalAmount;
        });

        this.products.forEach(element => {
            this.charges = element.deliveryCharge + this.charges;
        });
        if (this.totalPayble > 500) {
            this.charges = 0;
        }

    }

    //navigates to address page
    changeAddress() {
        this.route.navigate(['addresslist']);
    }

    //update value as per the selection
    updateSelectedValue(event, productPrice) {
        let updateQuantityPrice = productPrice * event.target.value;
        console.log(typeof event.target.value, "even data", typeof productPrice);
        this.totalPayble = this.totalAmount + updateQuantityPrice;
    }

    /** 
     * @description life cycle function to destroy subscription
    */
    ngDestroy() {
        if (this.productSub) {
            this.productSub.unsubscribe();
        }
    }
}
