import { Component, OnInit, ViewChild } from '@angular/core';
import { IonDatetime } from '@ionic/angular';
import * as moment from 'moment';

import { ModalController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Regex } from '../../lib/regex';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { TermsAndConditionComponent } from '../terms-and-condition/terms-and-condition.component';
import { Alert } from 'selenium-webdriver';
import { Store } from '@ngrx/store';
import { AuthStoreSelectors, AuthStoreActions } from '../../root-store';
import { GeneralService } from 'src/app/services/global services/general.service';
@Component({
    selector: 'app-registration',
    templateUrl: './registration.page.html',
    styleUrls: ['./registration.page.scss'],
})
export class RegistrationPage implements OnInit {
    registerationForm: FormGroup;
    @ViewChild('dobPicker') dobPicker: IonDatetime;
    dateOFBirth = '';
    passwordType: any;
    confirmPasswordType: any;
    termsAndCondChecked = '';
    constructor(private router: Router,
        public formBuilder: FormBuilder,
        public popoverController: PopoverController,
        private authStore: Store<AuthStoreSelectors.AuthState>,
        private generalService: GeneralService
        ) {
        this.registerationForm = this.formBuilder.group({
            firstName: ['', [Validators.required]],
            lastName: ['', [Validators.required]],
            password: ['', [Validators.required, Validators.pattern(Regex.PASSWORD)]],
            confirmPassword: ['', [Validators.required, Validators.pattern(Regex.PASSWORD)]],
            email: ['', [Validators.required, Validators.email]],
            phone_no: ['', [Validators.required, Validators.pattern(Regex.MOBILE_REGX)]],
            dob: ['', [Validators.required]],
            gender: ['male'],
            city: ['', [Validators.required]],
            nationality: ['', [Validators.required]],


            // captcha: ['', Validators.required]
        });
    }

    ngOnInit() {
        this.passwordType = 'password';
        this.confirmPasswordType = 'password';
        this.authStore.select(AuthStoreSelectors.selectRegisterIsLoading).subscribe((data)=>{
            this.generalService.setLoaderStatus(data)
            console.log('lader status', data )
          })
    }
    openDOBDatePicker() {
        this.dobPicker.open();
    }
    dobChanged(dateEvent) {
        // console.log(moment(dateEvent.detail.value).format('DD-MM-YYYY'));
        this.dateOFBirth = moment(dateEvent.detail.value).format('DD-MM-YYYY');
        this.registerationForm.get('dob').setValue(this.dateOFBirth)
    }

    onSubmit() {
        console.log('this.registerationForm', this.registerationForm);
        if (!this.registerationForm.valid) {
            this.registerationForm.markAllAsTouched();
            return;
            // this.registerationForm.
        }

        if (!this.termsAndCondChecked) {
            alert('Please accept the terms and condition')
            return;
        }

        let requestParam = this.getFormattedPayload(this.registerationForm.value);
        console.log('this.signUpForm.value', requestParam);
        // let formData = new FormData();
        // Object.keys(requestParam).forEach((ele, index) => {
        //     formData.append(ele, requestParam[ele]);
        // })
        //Registration action called with ngrx/store
        this.authStore.dispatch(new AuthStoreActions.Registration({ user: requestParam }));

        // this.router.navigate(['/login'])
    }

    checkTermsAndCond(event) {
        event.preventDefault();
        this.termsAndCondChecked = event.detail.checked
    }
    async openModal(ev: any) {

        const popover = await this.popoverController.create({
            component: TermsAndConditionComponent,
            cssClass: 'my-custom-class',
            event: ev,
            // translucent: true,
            keyboardClose: true
        });
        return await popover.present();
    }

    showPassword(type: string) {
        if (type == 'password') {
            this.passwordType = this.passwordType === 'password' ? this.passwordType = 'text' : this.passwordType = 'password';
        }
        else {
            this.confirmPasswordType = this.confirmPasswordType === 'password' ? this.confirmPasswordType = 'text' : this.confirmPasswordType = 'password';

        }
    }

    getFormattedPayload(userData) {
        return {
            'first_name': userData.firstName,
            'last_name': userData.lastName,
            'email': userData.email,
            'pass': userData.password,
            'confirmPass': userData.confirmPassword,
            'phone_no': userData.phone_no,
            'gender': userData.gender == 'male' ? '0' : '1',
        }
    }
}
