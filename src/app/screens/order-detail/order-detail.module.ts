import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { OrderDetailPageRoutingModule } from './order-detail-routing.module';
import { MomentModule } from 'ngx-moment';
import { OrderDetailPage } from './order-detail.page';
import { Downloader } from '@ionic-native/downloader/ngx';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    MomentModule,
    OrderDetailPageRoutingModule,
    // Downloader
  ],
  declarations: [OrderDetailPage]
})
export class OrderDetailPageModule {}
