import { Component, OnInit } from "@angular/core";
import { Router, ActivatedRoute } from "@angular/router";
import { Store } from "@ngrx/store";
import {
  ProfileStoreSelectors,
  ProfileStoreActions,
} from "src/app/root-store/profile-store";
import * as _ from "lodash";
import { Downloader, NotificationVisibility, DownloadRequest } from "@ionic-native/downloader/ngx";
import { throwError } from 'rxjs';
import { environment } from 'src/environments/environment';
import { GeneralService } from 'src/app/services/global services/general.service';
@Component({
  selector: "app-order-detail",
  templateUrl: "./order-detail.page.html",
  styleUrls: ["./order-detail.page.scss"],
})
export class OrderDetailPage implements OnInit {
  baseUrl = environment.CONFIG.BASE_URL;
  public receipt;
  public orderDetails;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private profileStore: Store<ProfileStoreSelectors.ProfileState>,
    private downloader: Downloader,
    private generalService: GeneralService
  ) {}

  async ngOnInit() {
    this.route.queryParams.subscribe((params) => {
      console.log(params);
    });
    await this.profileStore.select(ProfileStoreSelectors.selectLoader).subscribe((data)=>{
      this.generalService.setLoaderStatus(data)
      console.log('lader status', data )
    })
    
    await this.profileStore
      .select(ProfileStoreSelectors.selectOrderDetails)
      .subscribe(async (response) => {
        if (response && response.length > 0) {
          this.orderDetails = response;
          console.log("if");

          this.route.queryParams.subscribe((params) => {
            const filterData = _.filter(response, { _id: params.order_id });
            this.orderDetails = filterData[0];
            console.log("->", this.orderDetails);
          });
        } else {
          console.log("else");
          await this.profileStore.dispatch(
            new ProfileStoreActions.getOrderDetails()
          );
        }
      });

     
  }
  navigateToDetail = () => {
    this.router.navigate(["/order-detail"]);
  };
  downloadInvoice = async (data: any) => {
    const payload = {
      createdAt: data.product_details[0].createdAt,
      product_details: data.product_details,
      _id: data._id,
    };

    await this.profileStore.dispatch(new ProfileStoreActions.Invoice(payload));
    await this.profileStore
      .select(ProfileStoreSelectors.selectInvoice)
      .subscribe(async (response) => {
        // if(response)
        console.log("sdf-sdf", response);
        this.receipt=response.receipt
      });
const url = this.baseUrl+'/'+this.receipt
console.log(url)
      var request: DownloadRequest = {
        uri: url,
        title: 'Invoice.pdf',
        description: '',
        mimeType: '',
        visibleInDownloadsUi: true,
        notificationVisibility: NotificationVisibility.VisibleNotifyCompleted,
        destinationInExternalFilesDir: {
            dirType: 'Downloads',
            subPath: 'MyFile.apk'
        }
    };


this.downloader.download(request)
            .then((location: string) => console.log('File downloaded at:'+location))
            .catch((error: any) => console.error(error));
  };
}
