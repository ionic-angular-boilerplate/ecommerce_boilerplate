import { NgModule, APP_INITIALIZER, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { AccordionModule } from "ngx-accordion";
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { Globalization } from '@ionic-native/globalization/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { RootStoreModule } from './root-store/root-store.module';
import {
    SocialLoginModule,
    AuthServiceConfig,
    GoogleLoginProvider,
    FacebookLoginProvider,
} from "angular-6-social-login";
import { IonicStorageModule, Storage } from '@ionic/storage';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { AuthGuard, LoggedInAuthGuard } from './shared/models/guards';
import { Store, StoreModule } from '@ngrx/store';
import { AuthStoreSelectors, AuthStoreActions } from './root-store';
import { GeneralService } from './services/global services/general.service';
import { Push } from '@ionic-native/push/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { Downloader } from '@ionic-native/downloader/ngx';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { reducers } from './root-store/route-reducer';
import { environment } from 'src/environments/environment';
export function HttpLoaderFactory(http: HttpClient) {
    return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}
export function getAuthServiceConfigs() {
    let config = new AuthServiceConfig(
        [
            // {
            //     id: FacebookLoginProvider.PROVIDER_ID,
            //     provider: new FacebookLoginProvider("Your-Facebook-app-id")
            // },
            {
                id: GoogleLoginProvider.PROVIDER_ID,

                provider: new GoogleLoginProvider("784408118590-pks1oenr5fif2vrhvohv4dmrhas99c5m.apps.googleusercontent.com")
            }

        ]
    );
    return config;
}

export function rehydrateState(
    cache: Storage,
    store: Store<AuthStoreSelectors.AuthState>
): any {
    return async () => {
        try {
            const _user: any = await cache.get('authDetails');
            if (_user) {
                // console.log('user rehydrateState', _user);
                store.dispatch(new AuthStoreActions.RehydrateState({ data: _user }));
            }
            else {
                // console.log('Error rehydrateState', _user);
                store.dispatch(new AuthStoreActions.Logout());
            }
        } catch (e) {
            console.error('RehydrateState Error', e);
        }
    };
}

@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [
        BrowserModule,
        HttpClientModule,
        RootStoreModule,
        AccordionModule,
        IonicModule.forRoot(),
        IonicStorageModule.forRoot({
            name: '__mydb',
            driverOrder: ['sqlite', 'indexeddb', 'websql', 'localstorage']
        }),
        AppRoutingModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            },
            defaultLanguage: 'en'
        }),
        SocialLoginModule,
        StoreModule.forRoot(reducers),
    // Instrumentation must be imported after importing StoreModule (config is optional)
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),
    ],
    providers: [
        GeneralService,
        Globalization,
        StatusBar,
        SplashScreen,
        LoggedInAuthGuard,
        AuthGuard,
        Camera,
        Push,
        LocalNotifications,
        Downloader,
        {
            provide: AuthServiceConfig,
            useFactory: getAuthServiceConfigs
        },
        {
            provide: APP_INITIALIZER,
            useFactory: rehydrateState,
            deps: [Storage, Store],
            multi: true
        },
        { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
        // FCM
    ],
    bootstrap: [AppComponent],
    exports: [
        TranslateModule
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
