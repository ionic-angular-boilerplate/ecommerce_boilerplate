export const END_POINT = {
    LOGIN: '/login',
    REGISTER: '/register',
    GET_USER: '/getCustProfile',
    GET_CART: '/getCartProduct',
    ADD_TO_CART: '/addDataToCart',
    DELETE_CART_ITEM: '/deleteCartProduct',
    TOP_RATED: '/getAllProductsInHighestRating',
    ALL_CATEGORIES: '/getAllCategories',
    ORDERS: '/getOrderDetails',
    INVOICE: '/getInvoiceOfOrder',
    GET_PRODUCT: 'getProductByProdId'
}