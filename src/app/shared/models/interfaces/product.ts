export interface TopRatedProducts {
    _id: string,
    subImages_id: string,
    category_id: string,
    color_id: string,
    product_id: string,
    product_name: string,
    product_image: string,
    product_desc: string,
    product_rating: string,
    product_producer: string,
    product_cost: number,
    product_stock: number,
    product_dimension: string,
    product_material: string,
    createdAt: string,
    __v: number
}

export interface AllCategoryProduct {
    _id: string,
    category_name: string,
    product_image: string,
    category_id: string,
    created_at: string,
    __v: number
}