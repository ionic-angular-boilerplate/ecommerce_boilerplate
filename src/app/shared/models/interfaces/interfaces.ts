export interface User {
    authToken: string,
    profile: {
        lastname: string,
        firstname: string,
        email: string,
        dob: string,
        number: string,
        gender: string,
        profilepic: string
    }
}