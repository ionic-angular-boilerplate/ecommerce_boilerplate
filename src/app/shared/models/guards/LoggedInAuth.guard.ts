
import { Injectable } from "@angular/core";
import { CanActivate, Router, ActivatedRoute, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import { Store, select } from '@ngrx/store';
// import { UserStoreSelectors, UserStoreActions } from '../../root-store';
import { tap, map, first } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { GeneralService } from '../../../services/global services/general.service';
import { AuthStoreSelectors } from '../../../root-store';
// import { SplashScreenService } from '../services/splash-screen.service';

@Injectable()
export class LoggedInAuthGuard implements CanActivate {

    constructor(private _router: Router, private activatedRoute: ActivatedRoute,
        private store: Store<AuthStoreSelectors.AuthState>) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
        return this.store.pipe(
            select(AuthStoreSelectors.selectUserStatus),
            tap(loggedIn => {
                if (loggedIn) {
                    console.log('Inside if of LoggedInAuthGuard');
                    this._router.navigateByUrl('/tabs/home');
                    return false;
                }
                else {
                    console.log('Inside else of LoggedInAuthGuard');
                    return true;
                }
            }),
            first()
        )

    }

}