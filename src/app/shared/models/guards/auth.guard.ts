import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Observable, } from 'rxjs';

import { Store, select } from '@ngrx/store';
// import { UserStoreSelectors, UserStoreActions } from '../../root-store';
import { tap } from 'rxjs/operators';
import { GeneralService } from '../../../services/global services/general.service';
import { AuthStoreSelectors } from '../../../root-store';
import { first } from 'rxjs/internal/operators/first';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {
    allServicesCodes: Array<string> = [];
    constructor(public router: Router,
        private store: Store<AuthStoreSelectors.AuthState>) {
    }

    public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        return this.store
            .pipe(
                select(AuthStoreSelectors.selectUserStatus),
                tap(loggedIn => {
                    if (!loggedIn) {
                        console.log('Inside if of AuthGuard');
                        this.router.navigateByUrl('/login');
                        return false;
                    }
                    else {
                        // console.log('Inside else of AuthGuard');
                        return true;
                    }
                }), first()
            );
    }

}
