import { Component, OnInit } from '@angular/core';

import { Platform, ActionSheetController, AlertController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Globalization } from '@ionic-native/globalization/ngx';
import { UitilityService } from './services/global services/uitility.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';
import { AuthStoreActions, AuthStoreSelectors } from './root-store';
import { Store } from '@ngrx/store';
import { Subscription } from 'rxjs';
import { User } from './shared/models';
// import { FCM } from '@ionic-native/fcm/ngx';
// import { Firebase } from '@ionic-native/firebase/ngx';
import { Push, PushObject, PushOptions } from '@ionic-native/push/ngx';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { PushNotificationService } from './services/global services/push-notification.service';

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
    public selectedIndex = 0;
    public labelll = { value: '' };
    public isDarkmode: boolean = false;
    public profileSub: Subscription;
    userData: User;
    public appPages = [
        {
            title: 'Dashboard',
            url: '/tabs',
            icon: 'mail'
        },
        {
            title: 'Cart',
            url: '/cart',
            icon: 'cart'
        },
        {
            title: 'My Account',
            url: '/account',
            icon: 'person-circle'
        },

    ];

    constructor(
        private platform: Platform,
        private splashScreen: SplashScreen,
        private statusBar: StatusBar,
        private translate: TranslateService,
        private globalization: Globalization,
        private utilityService: UitilityService,
        private camera: Camera,
        public actionSheetController: ActionSheetController,
        public router: Router,
        private menu: MenuController,
        private store: Store<AuthStoreSelectors.AuthState>,
        // private fcm: FCM
        private localNotifications: LocalNotifications,
        private push: Push,
        public alertCtrl: AlertController,
        private pushService: PushNotificationService
    ) {
        this.initializeApp();
        this.utilityService.darkThemeEnabled.subscribe((data) => {
            this.isDarkmode = data;
        })
    }

    initializeApp() {
        this.platform.ready().then(() => {
            this.statusBar.styleDefault();
            this.splashScreen.hide();
            this.setDeviceDefaultLang(); // to set default language 
            this.getDeviceLanguage(); // to get default language 
        });
    }

    ngOnInit() {

        this.labelll.value = 'label'
        const path = window.location.pathname.split('/')[1];
        if (path !== undefined) {
            this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
        }

        //subscription of user profile
        this.profileSub = this.store.select(AuthStoreSelectors.selectUser).subscribe((data) => {
            if (data) {
                this.userData = data;
            }
        })
        this.pushService.initPushNotification()
    }


    /** 
     * @description to get device's default language
    */
    setDeviceDefaultLang() {
        if (localStorage.getItem('locale')) {
            const browserLang = localStorage.getItem('locale');
            this.translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');
        } else {
            localStorage.setItem('locale', 'en');
            this.translate.setDefaultLang('en');
        }
    }

    /** 
    * @description get called on chanage of the language
    * @param language: string language value e.g fr, en 
    **/
    changeLang(language: string) {
        localStorage.setItem('locale', language);
        this.translate.use(language);
    }

    /** 
    * @description to get preferred language
    **/
    getDeviceLanguage() {
        this.globalization.getPreferredLanguage()
            .then((res) => {
                this.translate.use(res.value.split('-')[0]);
            })
            .catch(e => console.log(e));

    }

    /** 
    * @description to chnage the theme
    **/
    changeTheme(event) {
        console.log('event', event);
        this.utilityService.enableDarkTheme(event.target.checked);

    }

    takephoto() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }
        this.camera.getPicture(options).then((imageData) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log('image', base64Image)
        }, (err) => {
            // Handle error
            console.log('imageErr', err)

        });
    }

    async openImgaeUploadOptions() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Albums',
            cssClass: 'my-custom-class',
            buttons: [
                {
                    text: 'Take photo',
                    role: 'destructive',
                    icon: 'camera',
                    handler: () => {
                        this.takephoto();
                    }
                },
                {
                    text: 'Choose photo from Gallery',
                    icon: 'image',
                    handler: () => {
                        this.openGallery();
                    }
                },
            ]
        });
        await actionSheet.present();
    }

    openGallery() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
        }

        this.camera.getPicture(options).then((imageData) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            const base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log(base64Image)

        }, (err) => {
            // Handle error
        })
    }
    logout() {
        this.menu.close()
        this.store.dispatch(new AuthStoreActions.Logout());
        // this.router.navigate(['/login'])
    }

    ngDestroy() {
        if (this.profileSub) {
            this.profileSub.unsubscribe();
        }
    }


}
