import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ActionSheetController } from '@ionic/angular';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
@Injectable({
    providedIn: 'root'
})
export class UitilityService {

    darkThemeEnabled: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
    constructor(
        private camera: Camera,
        public actionSheetController: ActionSheetController,
    ) {
        const preferDark = window.matchMedia('(prefers-color-scheme: dark)');
        this.enableDarkTheme(preferDark.matches);
        preferDark.addListener(mediaQuery => this.enableDarkTheme(mediaQuery.matches));
    }

    /**
      * @description handle the teheme mode.
      * @param shouldEnable - Boolean to apply dark class or not.
      */
    enableDarkTheme(shouldEnable: boolean) {
        this.darkThemeEnabled.next(shouldEnable);
        console.log('enableDarkTheme function ', shouldEnable);
        document.body.classList.toggle('dark', shouldEnable);
    }


    takephoto() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE
        }
        this.camera.getPicture(options).then((imageData) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            let base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log('image', base64Image)
        }, (err) => {
            // Handle error
            console.log('imageErr', err)

        });
    }

    async openImgaeUploadOptions() {
        const actionSheet = await this.actionSheetController.create({
            header: 'Albums',
            cssClass: 'my-custom-class',
            buttons: [
                {
                    text: 'Take photo',
                    role: 'destructive',
                    icon: 'camera',
                    handler: () => {
                        this.takephoto();
                    }
                },
                {
                    text: 'Choose photo from Gallery',
                    icon: 'image',
                    handler: () => {
                        this.openGallery();
                    }
                },
            ]
        });
        await actionSheet.present();
    }

    openGallery() {
        const options: CameraOptions = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE,
            sourceType: this.camera.PictureSourceType.SAVEDPHOTOALBUM
        }

        this.camera.getPicture(options).then((imageData) => {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64:
            const base64Image = 'data:image/jpeg;base64,' + imageData;
            console.log(base64Image)

        }, (err) => {
            // Handle error
        })
    }


}
