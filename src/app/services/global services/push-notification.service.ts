import { Injectable } from "@angular/core";
import { LocalNotifications } from "@ionic-native/local-notifications/ngx";
import { Platform, AlertController } from '@ionic/angular';
import { PushOptions, PushObject, Push } from '@ionic-native/push/ngx';
@Injectable({
  providedIn: "root",
})
export class PushNotificationService {
  constructor(
    private localNotifications: LocalNotifications,
    public alertCtrl: AlertController,
    private platform: Platform,
    private push: Push,
    ) {}


    // To display local notification 
  displayLocalNotification = () => {
    this.localNotifications.schedule({
      id: 1,
      title: "Local Notification",
      text: "Sample Notification",
    });
  };

  // for intializing the push notification 
  initPushNotification() {
    if (!this.platform.is("cordova")) {
      console.warn(
        "Push notifications not initialized. Cordova is not available - Run in physical device"
      );
      return;
    }
    const options: PushOptions = {
      android: {
        senderID: "97867280137", // can be found on the firebase console 
      },
      ios: {
        alert: "true",
        badge: false,
        sound: "true",
      },
      windows: {},
    };
    const pushObject: PushObject = this.push.init(options);

    pushObject.on("registration").subscribe((data: any) => {
      console.log("device token -> " + data.registrationId);
      //TODO - send device token to server
    });

    pushObject.on("notification").subscribe(async (data: any) => {
      console.log("message -> " + data.message);
      //if user using app and push notification comes
      if (data.additionalData.foreground) {
        console.log("foreground");
        // if application open, show the local notification 
        this.displayLocalNotification()
      } else {
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        // this.nav.push(DetailsPage, { message: data.message });
        console.log("Push notification clicked");
      }
    });

    pushObject
      .on("error")
      .subscribe((error) => console.error("Error with Push plugin" + error));
  }


  checkForPermission=()=>{
  //   this.push.hasPermission()
  // .then((res: any) => {

  //   if (res.isEnabled) {
  //     console.log('We have permission to send push notifications');
  //     this.initPushNotification()
  //   } else {
  //     console.log('We do not have permission to send push notifications');
  //   }

  // });
  }
  hit(data){
    data()
        console.log('function hits2')
        // data()
  }
}
