import { Injectable } from '@angular/core';
import { ToastController, LoadingController, AlertController } from '@ionic/angular';
import * as CryptoTS from 'crypto-ts'


const SECRET_KEY = 'test'
@Injectable({
    providedIn: 'root'
})
export class GeneralService {

    loader: any;
    showLoader: boolean = false;
    constructor(private toastCtrl: ToastController, private loadingCtrl: LoadingController, public alertController: AlertController) {

    }
    showToast(message, position, duration = 3000) {
        this.toastCtrl.create({
            message,
            duration,
            position
        }).then((toastData) => {
            toastData.present();
        });
    }

    async setLoaderStatus(flag = true) {
        console.log('flag', flag)
        if (flag) {
            this.showLoader = true;
            this.loader = null;
            await this.loadingCtrl.create(
                {
                    message: 'Loading...'
                }
            )
            .then(loader => {
                this.loader = loader;
                return loader.present().then(() => {
                    if (!this.showLoader) {
                        loader.dismiss();
                    }
                });
            });
        }
        else {
            this.showLoader = false;
           return  this.loadingCtrl.dismiss();
        }
    }

    encryptData(data: any) {
        let encryptedString = CryptoTS.AES.encrypt(JSON.stringify(data), SECRET_KEY);
        return encryptedString.toString()
    }

    decryptData(data: any) {
        let decryptedBytes;
        let decryptedText;
        decryptedBytes = CryptoTS.AES.decrypt(data, SECRET_KEY);
        decryptedText = decryptedBytes.toString(CryptoTS.enc.Utf8);
        return decryptedText
    }


    async presentAlertConfirm(header, message, callback) {
        const alert = await this.alertController.create({
            cssClass: 'my-custom-class',
            header: header,
            message: message,
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    cssClass: 'secondary',
                    handler: (blah) => {
                        return callback(false);
                    }
                }, {
                    text: 'Okay',
                    handler: () => {
                        return callback(true);
                    }
                }
            ]
        });

        await alert.present();
    }

    async presentAlert(header,message) {
        const alert = await this.alertController.create({
          cssClass: 'my-custom-class',
          header: header,
        //   subHeader: 'Subtitle',
          message: message,
          buttons: ['OK']
        });
    
        await alert.present();
      }
}
