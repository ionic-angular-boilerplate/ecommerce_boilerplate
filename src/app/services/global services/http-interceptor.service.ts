import { Injectable } from '@angular/core';
import {
    HttpInterceptor,
    HttpRequest,
    HttpResponse,
    HttpHandler,
    HttpEvent,
    HttpErrorResponse
} from '@angular/common/http';

import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        console.log('INTERCEPTOR');
        // We retrieve the token, if any
        // const token = 'ASDSACXXZ';
        // let newHeaders = req.headers;
        // if (token) {
        // If we have a token, we append it to our new headers
        //    newHeaders = newHeaders.append('authtoken', token);
        // }
        // Finally we have to clone our request with our new headers
        // This is required because HttpRequests are immutable
        // const authReq = req.clone({headers: newHeaders});
        // Then we return an Observable that will run the request
        // or pass it to the next interceptor if any
        return next.handle(req).pipe(
            map((event: HttpEvent<any>) => {
                if (event instanceof HttpResponse) {
                    return event;
                }
            }),
            catchError((error: HttpErrorResponse) => {
                return throwError(error);
            })
        )
    }
}
