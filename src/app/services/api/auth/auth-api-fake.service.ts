import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { END_POINT } from '../../../shared/models';
import { DatajsonService } from '../../datajson.service';
@Injectable({
    providedIn: 'root'
})
export class AuthApiCallService {
    BASE_URL = environment.CONFIG.BASE_URL;
    httpOptions = {
        headers: new HttpHeaders({
            'Content-Type': 'application/json'
        })
    };
    constructor(private http: HttpClient, private datajsonService: DatajsonService) {
    }

    /**
    * @description methods calls on the login action on native devices. Making Request for token.
    * @param payload : object of user credentails
    * @returns an object on the success/error response
    */
    login(payload): Observable<any> {
        console.log('payload', payload);
        this.datajsonService.loginResponse.profile.email = payload.email
        return new Observable((observer) => {
            observer.next(this.datajsonService.loginResponse),
                observer.complete()
        });
    }

    /**
     * @description method calls on registration action.
     * @param paylaod : object of user details
     * @returns object on the success/error response
     */
    registration(paylaod: any): Observable<any> {
        const url = `${this.BASE_URL}${END_POINT.REGISTER}`;
        console.log('Url', url, paylaod)
        return this.http.post(url, paylaod.user)
            .pipe(
                map((res: any) => {
                    console.log('registration Response', res);
                    return res;
                }),
                catchError(error => {
                    return throwError(error);
                })
            );
    }


}
