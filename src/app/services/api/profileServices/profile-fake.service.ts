import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { END_POINT } from '../../../shared/models';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable, of } from 'rxjs';
import { DatajsonService } from '../../datajson.service';

@Injectable({
    providedIn: 'root'
})
export class ProfileService {

    baseUrl = environment.CONFIG.BASE_URL;
    productID;
    constructor(public http: HttpClient, private datajsonService: DatajsonService) { }


    /**
    * @description methods calls product details by id API
    * @param product_id : product id
    * @returns an object on the success/error response
    */
    accessProductDetails(product_id) {// access product details
        return this.http.get(`${this.baseUrl}/getProductByProdId/${product_id}`);
    }


    /**
    * @description methods calls get cart API.
    * @returns an object on the success/error response
    */
    getCart(): Observable<any> {
        return of(this.datajsonService.cartProduct);
    }

    // /**
    //  * @description method calls delete API call
    //  * @param id: product id
    //  * @returns an object on the success/error response
    //  */
    // deteleCartProduct(id): Observable<any> {
    // const products = this.datajsonService.deleteFromCart(id);
    // return of(products);
    // }

    /** 
    * @description method to call top rated products
   */
    getTopRatedProducts() {
        return of(this.datajsonService.topRatedProduct);
    }

    /** 
     * @description method to call top rated products
    */
    getAllCategories() {
        return of(this.datajsonService.productCategory);
    }

}
