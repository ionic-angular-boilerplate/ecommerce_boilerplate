import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";

import { environment } from "../../../../environments/environment";
import { END_POINT } from "../../../shared/models";
import { map, catchError } from "rxjs/operators";
import { throwError, Observable, Subscription } from "rxjs";
import { DatajsonService } from "../../datajson.service";
import { Store } from "@ngrx/store";
import { AuthStoreSelectors } from "src/app/root-store";
import { GeneralService } from '../../global services/general.service';

@Injectable({
  providedIn: "root",
})
export class ProfileService {
  public profileSub: Subscription;
  token;
  baseUrl = environment.CONFIG.BASE_URL;
  selectAddress;
  editAddress;
  productID;
  constructor(
    public http: HttpClient,
    private datajsonService: DatajsonService,
    private store: Store<AuthStoreSelectors.AuthState>,
    private generalService: GeneralService
  ) {}

  /**
   * @description method to call order details
   */
  getOrders() {
    this.profileSub = this.store
      .select(AuthStoreSelectors.selectUser)
      .subscribe((data) => {
        if (data) {
          this.token = data.token;
        }
      });
    const httpOptions = {
      headers: new HttpHeaders({
        authorization: `bearer ${this.token}`,
      }),
    };
    const url = `${this.baseUrl}${END_POINT.ORDERS}`;
    return this.http.get(url,httpOptions).pipe(
      map((data: any) => {
        console.log("getOrders", data);
        return data;
      }),
      catchError((error: any) => {
          console.log("getOrders catchError", error);
          this.generalService.presentAlert('Error',error.error.message)
        return throwError(error.error);
      })
    );
  }

  downloadInvoice(payload: any) {
    console.log(payload);
    this.profileSub = this.store
      .select(AuthStoreSelectors.selectUser)
      .subscribe((data) => {
        if (data) {
          this.token = data.token;
        }
      });
    const httpOptions = {
      headers: new HttpHeaders({
        authorization: `bearer ${this.token}`,
      }),
    };
    const url = `${this.baseUrl}${END_POINT.INVOICE}`;
    return this.http.post(url, payload, httpOptions).pipe(
      map((data: any) => {
        console.log("invoice service", data);
        return data;
      }),
      catchError((error: any) => {
        console.log("invoice catchError", error);
        this.generalService.presentAlert('Error',error.error.message)

        return throwError(error.error);
      })
    );
  }
}
