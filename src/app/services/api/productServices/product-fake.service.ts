import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { END_POINT } from '../../../shared/models';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable, of } from 'rxjs';
import { DatajsonService } from '../../datajson.service';

@Injectable({
    providedIn: 'root'
})
export class ProductDetailsService {

    baseUrl = environment.CONFIG.BASE_URL;
    productID;
    constructor(public http: HttpClient, private datajsonService: DatajsonService) { }


    /**
    * @description methods calls product details by id API
    * @param product_id : product id
    * @returns an object on the success/error response
    */
    accessProductDetails(product_id) {// access product details
        return this.http.get(`${this.baseUrl}/${END_POINT.GET_PRODUCT}/${product_id}`);
    }

    /**
    * @description method calls product add to cart API.
    * @returns an object on the success/error response
    * @param payload : consist of product id and quantity
    */
    addToCart(payload): Observable<any> {
        // return of(this.datajsonService.cartProduct);
        let message_: string = '';

        let item: any = this.datajsonService.topRatedProduct.product_details.filter((data: any) => data._id == payload.product_id);

        if (item.length > 0) {
            // console.log('addToCart found', item);
            const selectedProduct = item.map((item) => ({
                ...item,
                quantity: payload.quantity
            }))
            const cart: any = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];

            if (cart && cart.length > 0) {
                // check wheather item already exist
                let exist: any = cart.filter((element: any) => element._id == selectedProduct[0]._id);

                if (exist.length > 0) {
                    console.log('PRODUCT ALREADY EXIST');
                    message_ = 'Already added to the cart.';
                }
                else {
                    cart.push(selectedProduct[0]);
                    localStorage.setItem('cart', JSON.stringify(cart));
                }
            }
            else {
                let cart: any = [];
                cart.push(selectedProduct[0]);
                localStorage.setItem('cart', JSON.stringify(cart));
            }
        }

        return of({ success: true, message: message_ });
    }



    /**
    * @description methods calls get cart API.
    * @returns an object on the success/error response
    */
    getCart(): Observable<any> {
        // return of(this.datajsonService.cartProduct);
        const cart: any = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
        if (cart.length > 0) {
            return of({ success: true, message: 'products in cart', product_details: cart });
        }
        else {
            return throwError({ error: { message: 'products are not available' } });
        }
    }

    /**
     * @description method calls delete API call
     * @param id: product id
     * @returns an object on the success/error response
     */
    deteleCartProduct(id): Observable<any> {
        // const products = this.datajsonService.deleteFromCart(id);
        // return of(products);
        // deleteFromCart(id) {
        const cart: any = localStorage.getItem('cart') ? JSON.parse(localStorage.getItem('cart')) : [];
        if (cart.length > 0) {
            cart.forEach((element, index) => {
                if (element._id == id) {
                    cart.splice(index, 1);
                    localStorage.setItem('cart', JSON.stringify(cart));
                }
            });
            return of({ success: true, message: 'products in cart', product_details: cart });
        }
        else {
            return throwError({ error: { message: 'products are not available' } });
        }
    }

    /** 
    * @description method to call top rated products
   */
    getTopRatedProducts() {
        return of(this.datajsonService.topRatedProduct);
    }

    /** 
     * @description method to call top rated products
    */
    getAllCategories() {
        return of(this.datajsonService.productCategory);
    }

}
