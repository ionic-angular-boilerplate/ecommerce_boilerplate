import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../../../environments/environment';
import { END_POINT } from '../../../shared/models';
import { map, catchError } from 'rxjs/operators';
import { throwError, Observable } from 'rxjs';
import { DatajsonService } from '../../datajson.service';
import { GeneralService } from '../../global services/general.service';

@Injectable({
    providedIn: 'root'
})
export class ProductDetailsService {

    baseUrl = environment.CONFIG.BASE_URL;
    selectAddress;
    editAddress;
    productID;
    constructor(public http: HttpClient, private datajsonService: DatajsonService,
        private generalService: GeneralService
    ) { }


    /**
    * @description methods calls product details by id API
    * @param product_id : product id
    * @returns an object on the success/error response
    */
    accessProductDetails(product_id) {// access product details
        return this.http.get(`${this.baseUrl}/${END_POINT.GET_PRODUCT}/${product_id}`);
    }

    /**
       * @description method calls product add to cart API.
       * @returns an object on the success/error response
       * @param payload : consist of product id and quantity
       */
    addToCart(payload) {
        const url = `${this.baseUrl}${END_POINT.ADD_TO_CART}`;
        const token = '';
        const httpOptions = {
            headers: new HttpHeaders({
                authorization: `bearer ${token}`,
            }),
        };
        return this.http.post(`${this.baseUrl}${END_POINT.ADD_TO_CART}`, payload, httpOptions);

    }

    /**
    * @description methods calls get cart API.
    * @returns an object on the success/error response
    */
    getCart() { //get product list from cart
        const url = `${this.baseUrl}${END_POINT.GET_CART}`;
        return this.http.get(url)
            .pipe(
                map((data: any) => {
                    console.log('getCart', data);
                    return data;
                }),
                catchError((error: any) => {
                    console.log('getCart catchError', error);
                    this.generalService.presentAlert('Error', error.error.message)

                    return throwError(error.error);
                })
            );
    }

    /**
     * @description method calls delete API call
     * @param id: product id
     * @returns an object on the success/error response
     */
    deteleCartProduct(id): Observable<any> {
        const url = `${this.baseUrl}${END_POINT.DELETE_CART_ITEM}?id=${id}`;
        return this.http.delete(url)
            .pipe(
                map((data: any) => {
                    return data;
                }),
                catchError((error: any) => {
                    this.generalService.presentAlert('Error', error.error.message)
                    return throwError(error.error);

                })
            );
    }

    /** 
     * @description method to call top rated products
    */
    getTopRatedProducts() {
        const url = `${this.baseUrl}${END_POINT.TOP_RATED}`;
        return this.http.get(url)
            .pipe(
                map((data: any) => {
                    console.log('getTopRatedProducts', data);
                    return data;
                }),
                catchError((error: any) => {
                    console.log('getTopRatedProducts catchError', error);
                    this.generalService.presentAlert('Error', error.error.message)

                    return throwError(error.error);
                })
            );
    }

    /** 
     * @description method to call top rated products
    */
    getAllCategories() {
        const url = `${this.baseUrl}${END_POINT.ALL_CATEGORIES}`;
        return this.http.get(url)
            .pipe(
                map((data: any) => {
                    console.log('getAllCategories', data);
                    return data;
                }),
                catchError((error: any) => {
                    console.log('getAllCategories catchError', error);
                    this.generalService.presentAlert('Error', error.error.message)

                    return throwError(error.error);
                })
            );
    }


}
