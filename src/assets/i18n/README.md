Steps to do basic setup for Multi-language translation, OR internationalization-localization in the Ionic-Angular application.

1. Create JSON files in src/assets/i18n folder like en.json (English), fr.json (French).
code snippet :
en.json 
    {
        "title": "Hello world",
        "description": "Ooohh .... did you just translate this text ?",
        "data": {"name": "My name is {{name_value}}"}
}
The {{value}} and {{name_value}} are kind of variable/constants we can pass from our component.

2. Implement ngx-translate library to detect and translate
// Install core library
npm install --save @ngx-translate/core

// Install http loader
npm install @ngx-translate/http-loader --save

3. Setup the library and http-loader
Add following function in our root module (app.module.ts). 

export function HttpLoaderFactory(http: HttpClient) {
return new TranslateHttpLoader(http, "./assets/i18n/", ".json");
}

4. Import the translation and http-loader modules in our root module (app.module.ts)
    TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            },
            defaultLanguage: 'en'
        })
5. Export the TranslateModule in (app.module.ts).

6. Implement Cordova Globalization plugin
//Install the plugin
ionic cordova plugin add cordova-plugin-globalization
npm install @ionic-native/globalization

7. Pass Globalization to the providers array in root module (app.module.ts).

8. Import TranslateService in app.component.ts to set default languge and declare it in the constructor as private translate: TranslateService

    this.translate.setDefaultLang('en');

9. Import the plugin in your app.component.ts declare it in the constructor as private globalization: Globalization

10. Call a simple function on your page start, which will set the device’s default language

    setDeviceLanguage() {
        if (localStorage.getItem('locale')) {  
                const browserLang = localStorage.getItem('locale');  
                this.translate.use(browserLang.match(/en|fr/) ? browserLang : 'en');  
              } else {  
                localStorage.setItem('locale', 'en');  
                this.translate.setDefaultLang('en');  
              }     
    }

11. To tell the translation service which language to translate the app.
    changeLang(language: string) {  
        localStorage.setItem('locale', language);  
        this.translate.use(language);  
      }

12. Following function on your page start, which will detect the device’s default language
    getDeviceLanguage() {
        this.globalization.getPreferredLanguage().then(res => {
            // Run other functions after getting device default lang
            this._initTranslate()
        })
    .catch(e => console.log(e));
    }

13. Update your templates.
    <h1>{{ 'title' | translate }}</h1>

- You can refer following link 
https://enappd.com/blog/how-to-translate-in-ionic-4-globalization-internationalization-and-localization/11/