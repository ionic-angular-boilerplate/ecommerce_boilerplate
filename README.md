# Ionic E-commence bioler plate app#

This application is a boiler plate of ionic e-commerce application. It is build using ionic 4 version with an angular.

### Don't know how to create ionic application ? ###
Following links will help you.

* [Learn creation process](https://ionicframework.com/docs/angular/your-first-app)
* [Learn deploying](https://ionicframework.com/docs/angular/your-first-app/6-deploying-mobile)
* [To know more](https://ionicframework.com/docs)

## Getting Started

To start building, clone this repository:

```bash
git clone https://bitbucket.org/ionic-angular-boilerplate/ecommerce_boilerplate.git
```

Once cloned, run the following to install dependencies and run the app:

```bash
npm i
npm start
```

### Concepts covered in the applications ###

## UI Pages and Components
- [Splash screen and App icon](/resources/README.md)
- [Login functionality](/src/app/screens/login/login.page.html)
- [Registration functionality](/src/app/screens/registration/registration.page.html)
- [Different views- slide, list, grid, image silder](/src/app/screens/home/home.page.html)
- [Drawer](/src/app/app.component.html)
- [Static page design-FAQ](/src/app/screens/settings/faq/faq.page.html)
- [Product List](/src/app/screens/product/product-list/product-list.page.html)
- [Product Search](/src/app/screens/search/search.page.html)
- [Product Details Page](/src/app/screens/product-details/product-details.page.html)
- [Cart page](/src/app/screens/order-detail/order-detail.page.html)
- [Checkout page](/src/app/screens/checkout/checkout.page.html)
- [Address page](/src/app/screens/address/address.module.ts)
- [My Profile screen](/src/app/screens/profile/profile.page.html)
- [My Order Screen](/src/app/screens/orders/orders.page.html)
- [Order details screen](/src/app/screens/order-detail/order-detail.page.html)


## Other things 
- [API end points](/src/app/shared/models/const/index.ts)
- [Interfaces ](/src/app/shared/models/interfaces/index.ts) 

## Global functionalities
- [Social login functionality](/src/app/screens/login/login.page.ts)
- [Upload image ,change image]()
- [Ionic storage functionality](/src/app/root-store/auth-store/effects.ts)
- [Localization or Translation](/src/assets/i18n/README.md)
- [Authguard](/src/app/shared/models/guards/auth.guard.ts)
- [Interceptor](/src/app/services/global services/http-interceptor.service.ts)
- [Form validation](/src/app/screens/registration/registration.page.html)
- [Ngrx - Store](/src/app/root-store)
- [REGEX](/src/app/lib/regex.ts)
- [Push notification](/src/app/app.component.ts)
- [Dark and light mode](/src/theme/README.md)
- [Share functionality](/src/app/screens/product-details/product-details.page.html)
- [Alert, confirm box, prompt, toast, encrypt-decrypt](/src/app/services/global services/general.service.ts)

